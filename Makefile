
SRCPATH='./src'
OUTPATH='./my-out'
JARPATH=$(OUTPATH)/EXE



all: compile2out


compile2out:
	$(MAKE) -C $(SRCPATH) all
	$(MAKE) -C $(SRCPATH) zip
	cd $(OUTPATH) && rm -fr EXE FONT
	mkdir -p $(OUTPATH)/EXE $(OUTPATH)/FONT
	mv $(SRCPATH)/ParaXafar.zip $(JARPATH)/ParaXafar.jar
	unzip $(JARPATH)/ParaXafar.jar -d $(OUTPATH)/FONT
	cp $(OUTPATH)/META-INF $(JARPATH) -r
	cd $(JARPATH) && zip ParaXafar.jar META-INF/*
	cd $(JARPATH) && rm -rf META-INF
	cp Documentation/ $(OUTPATH)/DOC -rf


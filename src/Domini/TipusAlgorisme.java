/**
 * @author Joan VInyals
 */

package Domini;

public enum TipusAlgorisme {
    //       Per mes informacio anar a les classes:
    JPEG, // AlgorismeJPEG
    LZW,  // AlgorismeLZW
    LZ78, // AlgorsimeLZ78
    LZSS  // AlgorismeLZSS
}
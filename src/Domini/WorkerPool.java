package Domini;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Function;

public class WorkerPool {

    private ForkJoinPool fjp;
    private ArrayList<Worker> workers;
    private int maxworkers;

    public WorkerPool(int maxworkers){
        fjp = new ForkJoinPool();
        workers = new ArrayList<>();
        this.maxworkers = maxworkers;
    }


    //despres de moltes proves, aquest metode amb el for es el que menys instruccions utilitza i menys overhead representa.
    public void WaitAll(){
        int size = workers.size();
        for(int i=0; i<size; ++i){
            //si algun thread no ha acabat, torna a recorrer l'array.
            if (!workers.get(i).hasfinished()) i = -1;
        }
    }

    public void Clear(){
        workers.clear();
    }

    public Object getWork(int index){
        return workers.get(index).getRes();
    }


    public <R> void DoWork(myFunction<R> func, ArrayList<Object> data){
        Worker<R> w = new Worker<>(func,data);
        workers.add(w);
        fjp.execute(w);
    }

}

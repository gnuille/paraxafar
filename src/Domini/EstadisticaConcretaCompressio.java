/**
 * @author Marc Clasca
 */
package Domini;

/**
 * \paragraph{} La subclasse \verb EstadisticaCompressio conté, a més a més de les propietats i mètodes que hereda de
 * la seva superclasse, la propietat \verb ratioCompressio  que ens indica el percentatge de reducció de la mida del
 * fitxer amb la compressió. És a dir, aplica el càlcul $$1 - (mida\_comprimit - mida\_original)$$ en la seva
 * constructora i posa el resultat en tipus \verb double  a la propietat esmentada.
 */
public class EstadisticaConcretaCompressio extends EstadisticaConcreta {

    private int size_orig;
    private int size_comp;

    /**
     * @param size_orig mida del fitxer original
     * @param size_comp mida del fitxer un cop comprimit
     */
    public EstadisticaConcretaCompressio(double temps, TipusAlgorisme a, int size_orig, int size_comp) {
        super(temps, a);
        this.size_orig = size_orig;
        this.size_comp = size_comp;
    }

    public double aconsegueixRatio() {
        return ((double) this.size_comp) / ((double) this.size_orig);
    }

}

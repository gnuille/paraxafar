/**
 * @author Joan Vinyals
 */

package Domini;

import java.util.ArrayList;

class Carpeta extends Fitxer{

    private ArrayList<Fitxer> contingut;

    Carpeta(String nom, String path) {
        super(nom, path);

        this.contingut = new ArrayList<Fitxer>();
    }

    void afegirFitxer(Fitxer f) {
        this.contingut.add(f);
    }
}

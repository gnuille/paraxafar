/**
 * @author Guillem Ramirez
 */

package Domini;

import Domini.util.ArbreBinariOrdenat;
import Domini.util.Parella;

import java.util.ArrayList;


public class AlgorismeLZSS extends Algorisme {
    private static final int MIDA_CERCA = 2048; //ofset = 11 bits
    private static final int MIDA_CONSULTA = 32; //len = 5 bits
    private static final int MIDA_MINIM_TROBALLA = 3; //tres carcters de troballa minima per a que comprimir surti "rentable"
    private ArbreBinariOrdenat paraules;
    private ArrayList<Character> comprimit;
    private char[] dades;
    private Parella<Integer, Integer> finestraCerca;
    private Parella<Integer, Integer> finestraConsulta;

    public AlgorismeLZSS() {
        paraules = new ArbreBinariOrdenat();
        finestraCerca = new Parella<Integer, Integer>(0, 0);
        finestraConsulta = new Parella<Integer, Integer>(0, MIDA_CONSULTA-1);
        comprimit = new ArrayList<Character>();
    }

    protected void comprimir(Fitxer fitxer) {
        dades = fitxer.aconseguirContingut();
        comprimirLZSS();
    }

    private void comprimirLZSS(){
        int contador = 0;
        int posicioBanderes = 0;
        char banderes = 0;
        while(true){

            //quin bit de bandera em de actualitzar?
            int index = contador%8;
            //si es el 0 cal una nova entrada
            if (index == 0){
                //afegim la proxima entrada de banderes
                comprimit.add((char) 0x00);
                //setejem l'anterior entrada de banderes
                comprimit.set(posicioBanderes, banderes);
                //posem a no comprimit per defecte
                banderes = 0;
                //actualitzem l'apuntador a la ultima entrada de banderes
                posicioBanderes = comprimit.size()-1;
            }
            //nombre de charaters que comprimim
            int desplaçament;
            //comprmim la entrada
            Parella<Integer, Integer> entradaComprimida = comprimirEntrada(dades);
            if (entradaComprimida == null){
                //l'entrada no s'ha pogut comprimir
                desplaçament = 1;
                //actualitzem la bandera amb entrada no comprimida
                //afegim el caracter sense comprimir
                comprimit.add(dades[finestraConsulta.aconseguirPrimer()]);
            }else{
                //l'entrada s'ha pogut comprimir
                //obtenim la llargaria de la troballa
                desplaçament = entradaComprimida.aconseguirSegon();
                //calculem la distancia des de l'element actual fins la troballa (valor maxim 2048)
                int clau = finestraConsulta.aconseguirPrimer() - entradaComprimida.aconseguirPrimer(); //la distancia des de la entrada que comprimim fins a la troballa
                //actualitzem la bandera com a entrada coprmida
                banderes = (char) (banderes | (1 << index));
                //l'entrada comprimida (11 bits de l'offset i 5 bits de llargada
                short entrada = (short) (((entradaComprimida.aconseguirSegon() & 0x1F) << 11) | (clau & 0X7FF));
                //afegim els segons 8 bits
                comprimit.add((char) (entrada >> 8));
                //afegim els primers 8 bits
                comprimit.add((char) (entrada & 0x0FF));
            }

            //per cada caracter que ha quedat fora de la finestra
            for (int i = finestraCerca.aconseguirPrimer(); i < finestraCerca.aconseguirPrimer() + desplaçament; i++){
                //si no hi cap
                if (finestraCerca.aconseguirSegon() > MIDA_CERCA) {
                    //esborrem la mes antiga
                    paraules.esborrar(i);
                }

                //actualitzem la finestra
                finestraCerca.establirSegon(finestraCerca.aconseguirSegon() + 1 );
                //creem la nova paraula a inserir
                String nova = "";
                for(int j = finestraCerca.aconseguirSegon(); j > finestraCerca.aconseguirSegon() - MIDA_CONSULTA && j >=0 && j < dades.length ; --j ) {
                    nova = dades[j] + nova;
                }
                //la inserim amb un apuntador global, es a dir, s'ha de modificar al guard-ho al fitxer comprimit
                paraules.inserir(nova, finestraCerca.aconseguirSegon()-MIDA_CONSULTA+1);

                //actualitzem la finestra de consulta de manera segura
                if( finestraConsulta.aconseguirSegon() <= dades.length ){
                    finestraConsulta.establirSegon(finestraConsulta.aconseguirSegon()+1);
                }
                if( finestraConsulta.aconseguirPrimer() <=  dades.length ){
                    finestraConsulta.establirPrimer(finestraConsulta.aconseguirPrimer()+1);
                }
            }
            //actualitzem la finestra de cerca
            if (finestraCerca.aconseguirSegon() > MIDA_CERCA){
                finestraCerca.establirPrimer(finestraCerca.aconseguirPrimer() +desplaçament);
            }

            contador++;
            //si hem acabat, guardem els continguts al buffer i retornem
            if (finestraConsulta.aconseguirPrimer() >=  dades.length) {
                char[] ret = new char[comprimit.size()];
                for (int i = 0; i<comprimit.size(); i++) ret[i] = comprimit.get(i);
                super.contingut = ret;
                //  paraules.log();
                return;
            }
        }

    }

    private Parella<Integer, Integer> comprimirEntrada(char [] dades){
        //calculem la troballa maxima a la que aspirem (31 caracters a partir del caracter que es vol comprimir)
        String maximaTroballa = "";
        for (int i = finestraConsulta.aconseguirPrimer(); i < finestraConsulta.aconseguirPrimer() + MIDA_CONSULTA; i++){
            if (i >= dades.length) break;
            maximaTroballa = maximaTroballa + dades[i];
        }
        //no fem la cerca si la maxima troballa a la que aspirem no es la mida minima
        if (maximaTroballa.length() < MIDA_MINIM_TROBALLA) return null;

        //la cerquem al arbre
        return  paraules.cercarMaximaCoincidenciaAmbMinim(maximaTroballa, MIDA_MINIM_TROBALLA);
    }

    protected void descomprimir(FitxerComprimit fitxerComprimit) {
        //aconseguim el contingut
        char[] dades = fitxerComprimit.aconseguirContingut();
        //Creem el buffer on afegirem les entrades descomprimides, també servira per calcular les troballes
        ArrayList<Character> descomprimit = new ArrayList<Character>(0);
//        char[] dades = super.contingut;
        int contador_entrades = 0;
        char banderes = 0;
        int apuntador = 0;
        while(true){
            //si ja em processat 8 elements, actualitzem les banderes i movem el apuntador
            if (contador_entrades%8 == 0) {
                banderes = dades[apuntador];
                apuntador++;
            }
            //calculem si l'entrada esta comprimida
            int comprimit = ((banderes >> (contador_entrades%8) )& 0x01);
            //si ho esta
            if (comprimit == 1){
                //apliquem mascaras per que java fa coses rares amb els chars
                dades[apuntador] = (char)  (dades[apuntador] & 0x00FF);
                dades[apuntador+1] = (char) (dades[apuntador+1] & 0x00FF);
                //calculem el desplaçament (mida de la troballa)
                int desplaçament = (dades[apuntador] >> 3);
                //calculem la distancia
                int id =(((dades[apuntador] & 0x07) << 8 )  ) | (dades[apuntador+1]);
                //computem la string de la troballa
                String tmp = "";
                for (int i = id+desplaçament-1; i >= id; i--){
                        //la distancia se li ha de sumar el desplaçament (nem al reves)
                        // i restar 1 ja que els array comencen en 0 i restar la distancia sobre el tamany maxim (caracter sobre el que estem comprimint)
                        tmp += descomprimit.get(descomprimit.size()-i+desplaçament-1);
//                        descomprimit.add(descomprimit.get(descomprimit.size() - i));
//                        System.out.print(descomprimit.get(i));
                }
                //afegim la string nova al buffer i movem lapuntador 1 vegada (per laltre caracter es fa posteriorment)
                for (int i = 0; i < tmp.length(); i++)  descomprimit.add(tmp.charAt(i));
                apuntador++;
            }
            else {
                //entrada no comprimida, l'agafem i cap al buffer
                descomprimit.add(dades[apuntador]);
            }
            //contem la entrada processada
            contador_entrades++;
            //incrementem l'apuntador
            apuntador++;
            //si em acabat de processar, guardem al buffer i retornem
            if(apuntador >= dades.length){
                char [] ret = new char[descomprimit.size()];
                for(int i = 0; i < descomprimit.size(); i++) ret[i] = descomprimit.get(i);
                super.contingut = ret;
                return;
            }

        }
    }

    public static void main(String args[]) {
        AlgorismeLZSS test = new AlgorismeLZSS();
        Fitxer perComprimir = null;
        test.comprimir(perComprimir);
        test.descomprimir(null);
    }


}

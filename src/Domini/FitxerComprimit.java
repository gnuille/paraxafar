/**
 * @author Joan Vinyals
 */

package Domini;

import Domini.util.CadenaDeBits;

class FitxerComprimit extends Fitxer {

    private Fitxer fitxerOriginal;

    FitxerComprimit(String nom, String path) {
        super(nom, path);
    }

    public void afegirAlgorisme(TipusAlgorisme a){
        char[] newcontingut = new char[contingut.length+1];
        switch (a){
            case JPEG : newcontingut[0]=0;break;
            case LZW  : newcontingut[0]=1;break;
            case LZ78 : newcontingut[0]=2;break;
            default   : newcontingut[0]=3;break; //LZSS
        }
        System.arraycopy(contingut,0,newcontingut,1,contingut.length);
        contingut = newcontingut;
    }
    public TipusAlgorisme extreureAlgorisme(){
        char algorisme = contingut[0];
        char[] newcontingut = new char[contingut.length-1];
        System.arraycopy(contingut,1,newcontingut,0,contingut.length-1);
        contingut = newcontingut;
        switch(algorisme){
            case 0: return TipusAlgorisme.JPEG;
            case 1: return TipusAlgorisme.LZW;
            case 2: return TipusAlgorisme.LZ78;
            default: return TipusAlgorisme.LZSS;
        }
    }

    public CadenaDeBits aconseguirCadenaDeBits() {
        CadenaDeBits cadena = new CadenaDeBits();
        cadena.afegirBits(super.aconseguirContingut());
        return cadena;
    }

    public void establirOriginal(Fitxer fo) {
        this.fitxerOriginal = fo;
    }

    public Fitxer aconseguirOriginal() {
        return this.fitxerOriginal;
    }
}

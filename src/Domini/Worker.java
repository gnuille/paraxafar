package Domini;


import java.util.ArrayList;
import java.util.concurrent.RecursiveAction;
import java.util.function.Function;


public class Worker<OUT> extends RecursiveAction {


    private myFunction<OUT> func;
    private ArrayList<Object> data;
    private OUT res;
    private boolean fin;

    //no s'uilitza pero m'agrda que hi hagi una constructora per defecte
    public Worker(){
        func = null;
        data = null;
        res = null;
        fin = false;
    }

    public Worker(myFunction<OUT> func, ArrayList<Object> data){
        this.func = func;
        this.data = data;
        this.res = null;
        this.fin = false;
    }


    public boolean hasfinished(){
        return fin;
    }
    public OUT getRes(){
        if (!fin){ //no hauria de passar mai, si es programa be (utilitzant el waitall
            return null;
        }
        return res;
    }


    public void compute(){ //metode que ens dona el  "RecursiveAction" (classe de java per paralelisme)
        fin = false;
        try {
            res = func.apply(data);
        }catch(Exception e){
            e.printStackTrace();
        }
        fin = true;
    }
}

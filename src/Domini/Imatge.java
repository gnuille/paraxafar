/**
 * @author Pablo Vizcaino
 */

package Domini;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Imatge extends Fitxer {

    private int amplada;
    private int altura;
    private char[] header;
    public Imatge(String nom, String path) {
        super(nom, path);
    }


    public int aconseguirAmplada() {return  amplada;}
    public int aconseguirAltura() {return  altura;}

    public char[] aconseguirHeader(){return header;}
    public char[] aconseguirData(){return contingut;}
    public char[] aconseguirContingut(){
        char[] output = new char[header.length+contingut.length];
        for(int i=0; i<header.length; ++i) output[i] = header[i];
        for(int i=0; i<contingut.length; ++i) output[i+header.length] = contingut[i];
        return output;
    }

    public void Llegir(){
        File f = new File(path);
        try {
            byte[] byte_contents = Files.readAllBytes(f.toPath());
            String string_contents = new String(byte_contents);
            String[] lines = string_contents.split("\n");

            String dimensions[] = lines[2].split(" ");
            this.amplada = Integer.parseInt(dimensions[0]);
            this.altura = Integer.parseInt(dimensions[1]);
            //System.out.println(this.amplada);
            //System.out.println(this.altura);
            int headersize = lines[0].getBytes().length +
                    lines[1].getBytes().length +
                    lines[2].getBytes().length +
                    lines[3].getBytes().length +
                    4; //4 x \n
            header = new char[headersize];
            for(int i=0; i<headersize; ++i) header[i] = (char)byte_contents[i];
            //header = Arrays.copyOfRange(byte_contents,0,headersize-1);
            byte[] data = Arrays.copyOfRange(byte_contents,headersize,byte_contents.length);
            contingut = new char[this.altura*this.amplada*3];
            for(int i=0; i<=data.length-3; i+=3){
                char R = (char)(data[i]&0x0FF);
                char G = (char)(data[i+1]&0x0FF);
                char B = (char)(data[i+2]&0x0FF);
                contingut[i]=R;
                contingut[i+1]=G;
                contingut[i+2]=B;
                //System.out.printf("RGB: %d %d %d \n",(int)R,(int)G,(int)B);
            }
            //Escriure();
        }catch(IOException e) {
            e.printStackTrace();
        }
        //System.out.printf("%d\n",contingut.length);
    }

    @Override
    public void establirContingut(char[] c) {
        String string_contents = new String(c);
        String[] lines = string_contents.split("\n");

        String dimensions[] = lines[2].split(" ");
        this.amplada = Integer.parseInt(dimensions[0]);
        this.altura = Integer.parseInt(dimensions[1]);
        int headersize = lines[0].getBytes().length +
                lines[1].getBytes().length +
                lines[2].getBytes().length +
                lines[3].getBytes().length +
                4; //4 x \n
        header = new char[headersize];
        for(int i=0; i<headersize; ++i) header[i] = c[i];
        //header = Arrays.copyOfRange(byte_contents,0,headersize-1);
        char[] data = Arrays.copyOfRange(c,headersize,c.length);
        contingut = new char[this.altura*this.amplada*3];
        for(int i=0; i<=data.length-3; i+=3){
            char R = (char)(data[i]&0x0FF);
            char G = (char)(data[i+1]&0x0FF);
            char B = (char)(data[i+2]&0x0FF);
            contingut[i]=R;
            contingut[i+1]=G;
            contingut[i+2]=B;
            //System.out.printf("RGB: %d %d %d \n",(int)R,(int)G,(int)B);
        }
        //Escriure();
    }

}

/**
 * @author Marc Clasca
 */

package Domini;

/**
 * Aquesta classe abstracta defineix el marc comú per les estadístiques concretes, és a dir, les estadístiques d'un
 * procés de compressió o descompressió.  Les classes filles d'\verb EstadisticaConcreta  tindran els següents elements:
 *
 * \begin{itemize}
 *     \item El \verb FitxerComprimit  associat al procés de compressió o descompressió.
 *     \item El tipus d'algorisme (implementat amb l'enum \verb TipusAlgorisme ) que s'ha utilitzat en aquest procés
 *     \item El temps que ha durat aquest procés
 * \end{itemize}
 *
 * També s'implementen, doncs, dues classes filles d'aquesta depenen del procés al qual ens referim:
 * \verb EstadisticaCompressio i \verb EstadisticaDescompressio .  Això és perquè la compressió conté altres indicadors
 * estadístics, com la ràtio de compressió.
 */
abstract class EstadisticaConcreta {

    private double temps;
    private TipusAlgorisme algorismeUtilitzat;

    /**
     * Crea un objecte estadística nou amb el temps i el fitxerComprimit
     * passats per paràmetre
     *
     * @param temps temps que ha trigat l'algorisme en completar-se
     * @return objecte Estadistica nou
     */
    public EstadisticaConcreta(double temps, TipusAlgorisme a) {
        this.temps = temps;
        this.algorismeUtilitzat = a;
    }

    public TipusAlgorisme aconseguirAlgorismeUtilitzat() {
        return this.algorismeUtilitzat;
    }

    public double aconsegueixTemps() { return this.temps; }

    /**
    public String aconsegueixCamiFitxerComprimit() {
        return this.fc.aconseguirCami();
    }
    */
}

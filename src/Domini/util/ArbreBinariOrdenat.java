package Domini.util;


/**
 * @author Guillem Ramirez
 */

//clase node, te punter a node esquerra i dreta, te les dades (string) i una clau (identificador)
class Node {
    int clau;
    String dades;
    Node esquerra;
    Node dreta;

    public Node(String dades, int clau){
        this.esquerra = this.dreta = null;
        this.dades = dades;
        this.clau = clau;
    }
}
//classe arbre binari ordenat, mante els nodes ordenats lexicogragicament de manera que la cerca es molt mes ràpida.
public class ArbreBinariOrdenat {
    private Node arrel;

    public ArbreBinariOrdenat(){
        this.arrel = null;
    }

    //inserim un node (log n)
    public void inserir(String dades, int clau){
        Node nou = new Node(dades, clau);
        if (this.arrel == null) {
            arrel = nou;
            return;
        }

        Node actual = this.arrel;
        Node pare = null;
        while(true){
            pare = actual;
            if (clauMesGran(actual, nou)){
               actual = actual.esquerra;
               if(actual == null){
                   pare.esquerra = nou;
                   return;
               }
            }else{
                actual = actual.dreta;
                if(actual == null){
                    pare.dreta = nou;
                    return;
                }
            }
        }
    }

    //esborra un node, cerca un successor que el mantingui ordenat
    public boolean esborrar(int clau){
        Parella<Node, Node> pareIFill = trobarNodeIPare(clau, null, this.arrel);
        //node no trobat al arbre, no esborrat
        Node aEsborrar = pareIFill.aconseguirPrimer();
        if (aEsborrar == null) return false;
        Node pare = pareIFill.aconseguirSegon();
        Boolean esEsquerra = esNodeEsquerra(aEsborrar);

        //el node a esborrar no te fills
        if (aEsborrar.dreta == null && aEsborrar.esquerra == null){
           if (aEsborrar == this.arrel ) {
               this.arrel = null;
           }
           if (esEsquerra){
               pare.esquerra = null;
           }else{
               pare.dreta = null;
           }
        }
        //el node a esborrar nomes te un fill
        else if(aEsborrar.dreta == null){
            if(aEsborrar == this.arrel) arrel = aEsborrar.esquerra;
            else if (esEsquerra) pare.esquerra = aEsborrar.esquerra;
            else pare.dreta = aEsborrar.esquerra;
        }else if(aEsborrar.esquerra == null){
           if(aEsborrar == this.arrel) arrel = aEsborrar.dreta;
           else if (esEsquerra) pare.esquerra = aEsborrar.dreta;
           else pare.dreta = aEsborrar.dreta;
        }else{
            Node successor = aconseguirSuccessor(aEsborrar);
            if(aEsborrar == arrel){
                arrel = successor;
            }else if(esEsquerra){
                pare.esquerra = successor;
            }else{
                pare.dreta = successor;
            }
        }
        return true;
    }

    //funcio feta servir per el esborrar per trobar un successor
    private Node aconseguirSuccessor(Node aEsborrar){
        Node successor = null;
        Node pareSuccessor = null;
        Node actual = aEsborrar.dreta;

        while (actual != null){
            pareSuccessor = successor;
            successor = actual;
            actual = actual.esquerra;
        }

        if (successor != aEsborrar.dreta){
            pareSuccessor.esquerra = successor.dreta;
            successor.dreta = aEsborrar.dreta;
        }

        return successor;
    }

    //cerca el node identificat per id, i retorna el node i el seu pare
    private Parella<Node, Node> trobarNodeIPare(int clau, Node pare, Node actual){
        Parella<Node, Node> ret;
        if (actual == null) ret = new Parella<Node, Node> (null, null);
        else if (actual.clau == clau) ret = new Parella<Node, Node> (actual, pare);
        else {

            ret = trobarNodeIPare(clau, actual, actual.esquerra);
            if(ret.aconseguirPrimer() == null || ret.aconseguirSegon() == null){
                ret = trobarNodeIPare(clau, actual, actual.dreta);
            }
        }
        return ret;
    }

    //funcio que diu si el node a es node esquerra crida a una funcio recursiva privada esNodeEsquerraCerca
    public boolean esNodeEsquerra(Node a){
        return esNodeEsquerraCerca(this.arrel, a, false);
    }

    //funcio recursiva, cerca el node, i es guarda si ve de l'esquerra o la dreta
    private Boolean esNodeEsquerraCerca(Node act, Node aCercar, boolean esEsquerra){
        if (act == null) return null;
        else if (act.clau == aCercar.clau) return esEsquerra;
        else {
            if (clauMesGran(act, aCercar) ){
                return esNodeEsquerraCerca(act.esquerra, aCercar, true);
            }else{
                return esNodeEsquerraCerca(act.dreta, aCercar, false);
            }
        }
    }

    //retorna cert si b < a fals en altre cas
    private boolean clauMesGran(Node a, Node b){
        return  a.dades.compareTo(b.dades) > 0;
    }

    //retorna cert si b < a fals en altre cas
    private boolean clauMesGran(String a, String b){
        return  a.compareTo(b) > 0;
    }

    //cerca la coincidencia mes gran de l'string a cercar en el arbre, si la coincidencia es menor que mida, retorna null
    public Parella<Integer, Integer>cercarMaximaCoincidenciaAmbMinim(String aCercar, int mida){
        Node n = cercarMaximaCoincidenciaEnNode(aCercar, arrel);
        Parella<Integer, Integer> aRetornar = null;
        if ( n != null && midaCoincidencia(n.dades, aCercar) > mida){
            aRetornar = new Parella<Integer, Integer>(n.clau, midaCoincidencia(n.dades, aCercar));
        }
        return aRetornar;
    }

    //donat un node i una string, cerca la coincidencia mes gran aprofitant que el node esta lexicographicament ordenat
    private Node cercarMaximaCoincidenciaEnNode(String aCerca, Node n){
        if (n == null) return null;
        if (n.esquerra == null && n.dreta == null) return n;
        else if(clauMesGran(n.dades, aCerca ) ){
            //esquerra
            if (n.esquerra != null){
                return cercarMaximaCoincidenciaEnNode(aCerca, n.esquerra);
            }else{
                return n;
            }
        }else{
            //dreta
            if (n.dreta != null){
                return cercarMaximaCoincidenciaEnNode(aCerca, n.dreta);
            }else{
                return n;
            }
        }
    }

    //funcio auxiliar, retorna la mida de la coincidencia entre dues stirngs
    private int midaCoincidencia(String a, String b){
        int i;
        for (i = 0; i < a.length() && i < b.length(); i++){
            if (a.charAt(i) != b.charAt(i)) return i;
        }
        return i;
    }
}



/**
 * @author Joan Vinyals
 */

package Domini.util;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Vector;

public class CadenaDeBits extends ArrayList<Character> {

    char falten;

    public CadenaDeBits() {
        super();
        this.falten = 0;
    }

    public CadenaDeBits(char[] valors) {
        this.afegirBits(valors);
    }

    public void afegirBits(int valor, int mida) {
        if (mida == 0) return;
        int mask = 1 << (mida-1);
        for (int i = 0; i < mida; i++) {
           if ( falten == 0 ) {
               falten = 8;
               super.add((char) 0);
           }
           int index_ultim = super.size() - 1;
           char ultim = super.get(index_ultim);
           if ( (valor & mask) != 0 ) {
               ultim = (char) (ultim | (1 << (falten -1) ) );
               super.set(index_ultim, ultim);
           }
           mask = mask >> 1;
           falten--;
        }
    }

    public void afegirBits(char[] valors) {
        if (falten == 0 || falten == 8) {
            for (char valor : valors) {
                super.add(valor);
            }
        } else {
            for (char valor : valors) {
                afegirBits(valor, 8);
            }
        }
    }

    public int aconsegueixMida() {
        return ((super.size()) * 8) - this.falten;
    }

    public Vector<Integer> aconseguirIntVector(int mida) {
        Vector<Integer> vector = new Vector<>();

        ListIterator<Character> list = super.listIterator();
        int current_bit = 0;
        int valor = 0;
        char current;
        while (list.hasNext()) {
            current = list.next();

            for (int i = 0; i < 8; i++) {
                valor <<= 1;
                valor |= ( ( current & (1 << 7) ) != 0 ? 1 : 0);
                current <<= 1;
                current_bit++;
                if (current_bit == mida) {
                    vector.add(valor);
                    valor = 0;
                    current_bit = 0;
                }
            }
        }
        return vector;
    }

    public char[] aconseguirCharArray() {
        char[] result = new char[super.size()];
        for (int i = 0; i < super.size(); i++) {
            result[i] = super.get(i);
        }
        return result;
    }

    public boolean aconseguirBit(int i) {
        Character b = super.get(i / 8);
        int bit = (b & (1 << (i % 8)));
        if (bit == 0) {
            return false;
        } else {
            return true;
        }

    }

    public void printCadena () {
        ListIterator<Character> list = super.listIterator();
        while(list.hasNext()) {
           int limit = list.hasNext() ? 8 : 8 - falten;
           char current = list.next();
           int mask = 1 << 7;
           for (int j = 0; j < limit; j++) {
               if ( (current & mask) != 0 ) {
                   System.out.print (1);
               } else {
                   System.out.print (0);
               }
               mask = mask >> 1;
           }
           System.out.print (" ");
        }
    }

    public void printByte() {
        for (int i = 0; i < super.size(); i++) {
        }
    }

   public static void main(String args[]) {
       CadenaDeBits proba = new CadenaDeBits();
       for (int i = 0; i < 10; i++) {
           proba.afegirBits(i, 12);
       }
       proba.printCadena();
   }
    
}






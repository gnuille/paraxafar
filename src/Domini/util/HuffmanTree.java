/**
 * @author Pablo Vizcaino
 */

package Domini.util;

public class HuffmanTree {

    private Short num;
    private Short freq;

    private HuffmanTree fill_esq;
    private HuffmanTree fill_dret;

    public HuffmanTree(Short num, Short freq){
        this.num = num;
        this.freq = freq;
        this.fill_esq = null;
        this.fill_dret = null;
    }

    public boolean teFills(){return (fill_esq!=null || fill_dret!=null);}

    public HuffmanTree AconseguirFillEsq() {return fill_esq;}
    public HuffmanTree AconseguirFillDret() {return fill_dret;}

    public Short AconseguirNum(){return num;}
    public Short AconseguirFreq(){return freq;}

    public void AfegirFillEsq(HuffmanTree e){fill_esq=e;}
    public void AfegirFillDret(HuffmanTree d){fill_dret=d;}
}

/**
 * @author Joan Vinyals
 */

package Domini.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

public class ArbreLZW<K,V> {

    private K clau;
    private V valor;

    private ArrayList<ArbreLZW<K, V>> fills;

    public ArbreLZW () {
      this.clau = null;
      this.valor= null;

      this.fills = new ArrayList<ArbreLZW<K, V>> ();
    }


    public ArbreLZW (K clau, V valor) {
        this.clau  = clau;
        this.valor = valor;

        this.fills = new ArrayList<ArbreLZW<K, V>> ();
    }

    public void afegirFill(ArbreLZW<K, V> a) {
      if (this.aconseguirFill(a.aconseguirClau()) == null) {
        this.fills.add(a);
      }
    }

    public void afegirFill(K clau, V valor) {
      if (this.aconseguirFill(clau) == null) {
        this.fills.add(new ArbreLZW<K, V>(clau, valor));
      }
    }

    public ArbreLZW aconseguirFill(K clau) {
      for (ArbreLZW<K, V> fill : fills) {
        if (fill.aconseguirClau() == clau) {
          return fill;
        }
      }
      return null;
    }

    public K aconseguirClau() {
      return this.clau;
    }

    public V aconseguirValor() {
      return this.valor;
    }

    public boolean teFills() { return !this.fills.isEmpty(); }

}

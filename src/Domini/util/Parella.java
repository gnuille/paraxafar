/**
 * @author Guillem Ramirez 
 */

package Domini.util;

public class Parella<T, K> {
   private T i;
   private K j;

   public Parella(){
      i = null;
      j = null;
   }

   public Parella(T i, K j){
      this.i = i;
      this.j = j;
   }

   public T aconseguirPrimer(){
      return this.i;
   }

   public K aconseguirSegon(){
      return this.j;
   }

   public void establirPrimer(T i){
     this.i = i;
   }

   public void establirSegon(K j){
      this.j = j;
   }
}

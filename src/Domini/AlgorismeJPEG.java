/**
 * @author Pablo Vizcaino
 */

package Domini;


import Domini.util.HuffmanTree;

import javax.lang.model.type.NullType;
import java.util.*;

class AlgorismeJPEG extends Algorisme {
    private double bigratio = 4.0; //Qmatrix es multiplica per aquest valor per aconseguie major compressió
    private short[] Qmatrix = {16,11,10,16,24,40,51,61,
                             12,12,14,19,26,58,60,55,
                             14,13,16,24,40,57,69,56,
                             14,17,22,29,51,87,80,62,
                             18,22,37,56,68,109,103,77,
                             24,35,55,64,81,104,113,92,
                             49,64,78,87,103,121,120,101,
                             72,92,95,98,112,100,103,99};

    public AlgorismeJPEG(){
    }


    private int numthreads;
    public AlgorismeJPEG(int numthreads){
        this.numthreads = numthreads;
    }
    private double[] ratios;
    //constructora amb ratios de compressió
    public AlgorismeJPEG(double ratio_Y, double ratio_Cb, double ratio_Cr, int numthreads){
        this.numthreads = numthreads;
        double[] r =  {ratio_Y,ratio_Cb,ratio_Cr};
        ratios = r;
    }

    public void comprimir(Fitxer fitxer) {
        if (this.numthreads==0) this.numthreads = 1;
        //Tractem el fitxer com el que és, una imatge
        Imatge img = (Imatge)fitxer;
        //Separem header i dades
        char[] header = img.aconseguirHeader();
        char[] data = img.aconseguirData();

        //aconseguim el número de blocs
        int amplada = img.aconseguirAmplada();
        int altura = img.aconseguirAltura();
        int blocks_x = amplada/8;
        int blocks_y = altura/8;
        int extra_x = (amplada % 8);
        int extra_y = (altura % 8);
        if(extra_x!=0) {
            ++blocks_x;
        }
        if(extra_y!=0) {
            ++blocks_y;
        }


        //conversió a YCbCr
        char[] YCbCr = RGBtoYCbCr(data);
        //sepració en canals
        char[] Y = getChannel(YCbCr,0);
        char[] Cb = getChannel(YCbCr,1);
        char[] Cr = getChannel(YCbCr,2);
        //Blocking
        char[][] blocked_Y = getBlocks(Y,blocks_x,blocks_y,extra_x,extra_y);
        char[][] blocked_Cb = getBlocks(Cb,blocks_x,blocks_y,extra_x,extra_y);
        char[][] blocked_Cr = getBlocks(Cr,blocks_x,blocks_y,extra_x,extra_y);
        //Tractem els blocs
        contingut = new char[0];

        //Creem una worker pool que s'encarrega de gestionar els threads
        WorkerPool wp = new WorkerPool(numthreads);

        for(int y=0; y<blocks_y; y+=numthreads){
            //paralelisme a nivell de files de blocs. Un thread fa tota una fila. Si fa només un bloc, l'acaba massa rapid i hi ha més overhead que benefici.
            if(numthreads>1) { //si hi ha paralelisme...

                //calculem els threads que utilitzarem (en principi numthreads, pero la feina restant es menor a numthreads, ens adaptem.
                int actualthreads = numthreads;
                if (blocks_y-y < numthreads) actualthreads = blocks_y - y;

                //creem els workers (threads).
                for (int w = 0; w < actualthreads; ++w) {
                    ArrayList<Object> params = new ArrayList<>(Arrays.asList(blocked_Y, blocked_Cb, blocked_Cr, y+w,blocks_x) );
                    wp.DoWork(this::comprimirFilaBlockThread, params);
                }
                //esperem a que acabin tots
                wp.WaitAll();
                //preparem una arraylist amb els resultats que voldrem juntar
                ArrayList<char[]> continguts = new ArrayList<>();
                continguts.add(contingut); //tota la feina ja feta anteriorment
                int n_cont = contingut.length;
                //afegim a l'arraylist la feina dels threads
                for (int w = 0; w < actualthreads; ++w) {
                    char[] tmp = (char[])wp.getWork(w);
                    continguts.add(tmp);
                    n_cont += tmp.length;
                }
                wp.Clear(); //borrem els workers
                contingut = appendNArrays(continguts,n_cont); //juntem tots els resultats
            }else{ //non-paralel
                //per comoditat utilitzo la mateixa funcio que quan es fa paralel, pero sense crear threads.
                ArrayList<Object> params = new ArrayList<>(Arrays.asList(blocked_Y, blocked_Cb, blocked_Cr, y, blocks_x));
                contingut = appendArrays(contingut, comprimirFilaBlockThread(params));
            }

           // }
        }
        //al acabar, guardem els ratios de compressió i el header
        char[] charratios = {(char)(ratios[0]*255),(char)(ratios[1]*255),(char)(ratios[2]*255)};
        ArrayList<char[]> appendarrays = new ArrayList<>();
        appendarrays.add(header); appendarrays.add(charratios); appendarrays.add(contingut);
        contingut = appendNArrays(appendarrays,header.length+charratios.length+contingut.length);
    }



    public char[] comprimirFilaBlockThread(ArrayList<Object> params){
        //agafa els parametres
        char[][] blocked_Y = (char[][])params.get(0);
        char[][] blocked_Cb = (char[][])params.get(1);
        char[][] blocked_Cr = (char[][])params.get(2);
        int fila = (int)params.get(3);
        int blocks_x = (int)params.get(4);

        ArrayList<char[]> continguts = new ArrayList<>();
        int n_contingut = 0;
        for(int i=0; i<blocks_x; ++i) {

            char[] compressedBlock_Y = comprimirCanal(blocked_Y[fila*blocks_x + i], (ratios[0]));
            char[] compressedBlock_Cb = comprimirCanal(blocked_Cb[fila*blocks_x + i], (ratios[1]));
            char[] compressedBlock_Cr = comprimirCanal(blocked_Cr[fila*blocks_x + i], (ratios[2]));

            //Calculem tamany i header: crec que podria ser mes petit
            int size = compressedBlock_Y.length + compressedBlock_Cb.length + compressedBlock_Cr.length;
            char[] head = new char[4];
            head[3] = (char) (size & 0x0ff);
            head[2] = (char) ((size & 0x0ff00) >> 8);
            head[1] = (char) ((size & 0x0ff0000) >> 32);
            head[0] = (char) ((size & 0x0ff000000) >> 48);
            //Afegim els resultats a la llista
            continguts.add(head); continguts.add(compressedBlock_Y);
            continguts.add(compressedBlock_Cb); continguts.add(compressedBlock_Cr);
            n_contingut += (size +4 );
        }
        //retornem tots els resultats junts (la fila)
        return appendNArrays(continguts,n_contingut);
    }


    //es fan accesos lineals a data i es va decidint a quin bloc va.
    //és te en compte extra_X i extra_Y pel padding
    public char[][] getBlocks(char[] data, int blocks_x, int blocks_y, int extra_x, int extra_y){

        char[][] output = new char[blocks_y*blocks_x][8*8];
        int index = 0;
        int subfiles = 8;
        int subcolumnes;
        //per cada fila de blocs
        for(int y=0; y<blocks_y; ++y){
            //per cada subfila de les 8 dins la fila de blocs. Si l'altura no es multiple de 8, en l'ultima fila de blocs no seran 8.
            if(y==blocks_y-1 && extra_y!=0) subfiles = extra_y;
            for(int i=0; i<subfiles; ++i) {
                //per cada bloc X
                for (int x = 0; x < blocks_x; ++x) {
                    int index_block = y*blocks_x + x;
                    //agafem les 8 dades de la fila. Si l'amplada no es multiple de 8, no seran 8.
                    if(x==blocks_x-1 && extra_x != 0) subcolumnes = extra_x;
                    else subcolumnes = 8;
                    for(int k=0; k<subcolumnes; ++k) {
                        output[index_block][(i*8)+k] = data[index++]; //explotem localitat espacial de data fent accesos sequencials!
                    }
                }
            }
        }

        return output;

    }


    //algorisme JPEG en si per cada bloc 8x8
    private char[] comprimirCanal(char[] input, double ratio){
        //Els pasos millor llegir-los al document d'entrega
        byte[] processed = PreProcessing(input);
        double[] transformed = DCT(processed);
        short[] quantizated = Quantization(transformed,ratio);
        short[] zigzaged = zigzag(quantizated);
        short[] packed = pack(zigzaged);
        Map<Short,String> HuffManTable = CreateHuffman(packed);
        char[] encoded = Encode(HuffManTable,packed);
        return encoded;
    }

    //procés de descompressió
    protected void descomprimir(FitxerComprimit fitxerComprimit) {
        if (this.numthreads==0) this.numthreads = 1;
        //Obtenir el contingut del fitxercomprimit
        char[] compressed = fitxerComprimit.aconseguirContingut();
        //Separar el header
        char[] header = getHeader(compressed);
        //obtenir el numero de blocs i el ratio de compressio
        int dimensions[] = decompressDimensions(header);
        int blocks_x = dimensions[0]/8;
        int blocks_y = dimensions[1]/8;
        int extra_x = dimensions[0]%8;
        int extra_y = dimensions[1]%8;
        //System.out.printf("EXTRA X : %d, EXTRA_Y : %d\n",extra_x,extra_y);
        if (extra_x!=0) ++blocks_x;
        if (extra_y!=0) ++blocks_y;
        ratios = decompressRatios(compressed);
        //Descomprimir bloc a bloc
        char[][] uncompressed_blocks = new char[blocks_x*blocks_y][8*8*3];

        //Crea pool de workers
        WorkerPool wp = new WorkerPool(numthreads);
        for(int y=0; y<blocks_y; ++y){
            int x;
            for(x=0; x<blocks_x; x+=numthreads){ //threads a nivell de bloc. Es més senzill que per files com abans
                if(numthreads>1) {
                    //com abans, calcula si ha de fer servir menys threads
                    int actualthreads = numthreads;
                    if (blocks_x-x < numthreads) actualthreads = blocks_x - x;
                    //els hi dona feina
                    for (int w = 0; w < actualthreads; ++w) {
                        ArrayList<Object> params = new ArrayList<>(Arrays.asList(getBlock(compressed,y,x+w,blocks_y,blocks_x)));
                        wp.DoWork(this::descomprimirBlockThread,params);
                    }
                    //espera a que acabin
                    wp.WaitAll();
                    //guarda la feina feina
                    for (int w = 0; w < actualthreads; ++w) {
                        uncompressed_blocks[y*blocks_x + x + w] = (char[]) wp.getWork(w);
                    }
                    wp.Clear();

                }else{ //non-paralel
                    ArrayList<Object> params = new ArrayList<>(Arrays.asList(getBlock(compressed,y,x,blocks_y,blocks_x)));
                    uncompressed_blocks[y*blocks_x + x] = descomprimirBlockThread(params);
                }
            }
        }

        //Passar de un llistat de blocs descomprimits a el format original de la imatge
        contingut = deBlock(uncompressed_blocks,blocks_x,blocks_y,extra_x,extra_y);
        //afegir el header
        contingut = appendArrays(header,contingut);
    }

    public char[] descomprimirBlockThread(ArrayList<Object> params) {
        char[] compressed_block = (char[])params.get(0);
        //Descomprimir cada canal
        char[] uncompressed_Y = descomprimirCanal(compressed_block, 0, ratios[0]);
        char[] uncompressed_Cb = descomprimirCanal(compressed_block, 1, ratios[1]);
        char[] uncompressed_Cr = descomprimirCanal(compressed_block, 2, ratios[2]);
        //Ajuntar-los convertint-los en RGB
        return YCbCrtoRGB(uncompressed_Y, uncompressed_Cb, uncompressed_Cr);
    }

    //Obtenir les dimensions del fitxer comprimit
    private int[] decompressDimensions(char[] header){
        int newlines = 0;
        int index = 0;
        //Saltarse el header que no interessa
        while(newlines<2) {
            if (header[index]=='\n') ++ newlines;
            ++index;
        }
        //amplada
        int ret1 = 0;
        while(header[index]!=' '){
            ret1 = ret1*10 + ((header[index++]-'0')&0x0ff);
        }
        //altura
        ++index;
        int ret2 = 0;
        while(header[index]!='\n'){
            ret2 = ret2*10 + ((header[index++]-'0')&0x0ff);
        }
        int[] output = {ret1,ret2};
        return output;
    }
    //obtenir els ratios
    private double[] decompressRatios(char[] input){
        double output[] = new double[3];
        //skip header
        int lines = 0;
        int offset = 0;
        while(lines < 4){
            if (input[offset] == '\n') ++lines;
            ++offset;
        }
        output[0] = (input[offset++]&0x0ff)/255.0;
        output[1] = (input[offset++]&0x0ff)/255.0;
        output[2] = (input[offset]&0x0ff)/255.0;
        return output;
    }
    //Obtenir els tres canals que formen un bloc
    private char[] getBlock(char [] input, int y, int x, int blocks_y, int blocks_x){
        //skip header
        int lines = 0;
        int offset = 0;
        while(lines < 4){
            if (input[offset] == '\n') ++lines;
            ++offset;
        }
        offset += 3; //skip ratios
        //Va saltant els blocs amb la seva mida, fins arribar al que vol
        int numblock = y*blocks_x + x;
        int blocksize = 0;
        for (int i=0; i<=numblock; ++i) {
            blocksize = (input[offset++]&0x0ff)<<48;
            blocksize += (input[offset++]&0x0ff)<<36;
            blocksize += (input[offset++]&0x0ff)<<8;
            blocksize += (input[offset]&0x0ff);
            offset += blocksize + 1;
        }
        //com ha acabat sumant offset += blocksize -1, ho desfà.
        offset = offset - blocksize;
        char[] output = new char[blocksize];
        for (int i=0; i<blocksize; ++i) output[i] = input[offset+i];
        return output;
    }
    //Descomprimir el canal. L'invers de l'algorisme JPEG
    private char[] descomprimirCanal(char[] input, int canal, double ratio){
        char[] splited = splitCompression(input,canal);
        short[] decoded = Decode(splited);
        short[] unpacked = inverse_pack(decoded);
        short[] inv_zigzag = inverse_zigzag(unpacked);
        short[] dequant = InverseQuantization(inv_zigzag,ratio);
        short[] detransformed = InverseDCT(dequant);
        return InversePreProcessing(detransformed);
    }

    //Convertir una matriu de blocs en una organització com la imatge ppm.
    private char[] deBlock(char[][] input, int blocks_x, int blocks_y, int extra_x, int extra_y){
        //char[] output = new char[input.length*input[0].length];
        ArrayList<Character> out = new ArrayList<>();
        int index = 0;
        //per cada fila de blocs
        int subfiles = 8;
        int subcolumnes;
        for(int y=0; y<blocks_y; ++y){
            //per cada subfila dins la fila de blocs. si és l'ultima fila de blocs i l'alçada%8!=0, no hi ha 8 subfiles
            if (y==blocks_y-1 && extra_y!=0) subfiles = extra_y;
            for(int i=0; i<subfiles; ++i) {
                //per cada bloc X
                for (int x = 0; x < blocks_x; ++x) {
                    int index_block = y*blocks_x + x;
                    //agafem les 8*3 dades de la fila. si és la columna dreta de blocs i l'amplada%8!=0, no hi ha 8*3 dades
                    if (x==blocks_x-1 && extra_x!=0) subcolumnes = extra_x*3;
                    else subcolumnes = 8*3;
                    for(int k=0; k<subcolumnes; ++k) {
                        //output[index++] = input[index_block][(i*8*3)+k];
                        out.add(input[index_block][(i*8*3)+k]);
                    }
                }
            }
        }
        char[] output = new char[out.size()];
        for(int i=0; i<out.size();++i) output[i]=out.get(i);
        return output;
    }

    //Conversió RGB a YCbCr
    private char[] RGBtoYCbCr(char[] RGBpixels){

        int size = RGBpixels.length;
        char[] YCbCrpixels = new char[size];
        char r,g,b,y,cb,cr;
        for (int i=0; i<=size-3; i+=3){
            r = RGBpixels[i]; g = RGBpixels[i+1]; b = RGBpixels[i+2];

            y = (char)(16 + (char)((65.738*r+129.057*g+25.064*b)/256.0));
            cb = (char)(128 + (char)((-37.945*r-74.494*g+112.439*b)/256.0));
            cr = (char)(128 + (char)((112.439*r-97.154*g-18.285*b)/256.0));

            YCbCrpixels[i]   = y;
            YCbCrpixels[i+1] = cb;
            YCbCrpixels[i+2] = cr;
        }
        return YCbCrpixels;
    }
    //conversió YCbCr a RGB
    private char[] YCbCrtoRGB (char[] Y, char[] Cb, char[] Cr){
        int size = Y.length;
        char[] RGBpixels = new char[size*3];
        int y,cb,cr,r,g,b;
        for (int i=0; i<size; i++){
            y = Y[i]; cb = Cb[i]; cr = Cr[i];
            r = (int) ((298.082*(y - 16) + 408.583*(cr - 128))/256.0);
            g = (int) ((298.082*(y - 16) - 100.291*(cb - 128) - 208.120*(cr - 128))/256.0);
            b = (int) ((298.082*(y - 16) + 516.411*(cb - 128))/256.0);
            //Anem amb compte que per errors de floating point no sortim de rang
            if (r>255) r = 255;
            else if (r<0) r = 0;
            if (g>255) g = 255;
            else if (g<0) g = 0;
            if (b>255) b = 255;
            else if (b<0) b = 0;

            RGBpixels[3*i]   = (char)r;
            RGBpixels[3*i+1] = (char)g;
            RGBpixels[3*i+2] = (char)b;
        }
        return RGBpixels;
    }
    //Donada un vector amb paquets de 3 elements (pixels), retorna un vector amb només un dels 3 canals.
    private char[] getChannel(char [] input, int n){
        int size = input.length/3;
        char[] output = new char[size];
        for(int i=0; i<size; ++i){
            output[i] = input[3*i + n];
        }
        return output;
    }
    //Resta 127 a tots els elements i ho converteix a byte
    private byte[] PreProcessing(char [] input){
        int size = input.length;
        byte[] output = new byte[size];
        for(int i=0; i<size; ++i) output[i] = (byte)(input[i]-127);
        //for(int i=0; i<size; ++i) output[i] = (short)(input[i]-0);
        return output;
    }
    //Suma 127 a tots els elements i ho converteix a char
    private char[] InversePreProcessing(short [] input){
        int size = input.length;
        char[] output = new char[size];
        for(int i=0; i<size; ++i) output[i] = (char)(input[i]+127);
        //for(int i=0; i<size; ++i) output[i] = (char)(input[i]+0);
        return output;
    }

    //s'escull entre optimitzat i no optimitzar (al compilar, per provar)
    private double[] DCT(byte [] input){
        //return DCT2D(input);
        return DCTopt(input);
    }

    //primer transforma files i despres columnes
    private double[] DCTopt(byte[] input){
        double[] output = new double[64];

        double[] rows = new double[64];
        for(int i=0; i<64;++i)rows[i]=(double)input[i];

        for (int i=0; i<8; ++i){
            double row[] = DCT1D(rows,i*8,8,1);
            for(int k=0; k<8; ++k) rows[i*8 + k] = row[k];
        }
        for (int j=0; j<8; ++j){
            double[] col = DCT1D(rows,j,8,8);
            for(int k=0; k<8;++k) output[k*8 + j] = col[k];
        }
        //PrintDoubleMatrix(output,8,8);
        return output;
    }

    //per comoditat, se li pasa tot el bloc de 8 per 8. amb i,n,stride s'accedeix nomes a una fila o una columna
    private double[] DCT1D(double [] input, int i, int n, int stride){
        double partial_sum;
        double[] output = new double[n];
        for (int k=0; k<n; ++k){ //Per cada un dels 8 valors que retornem
            partial_sum = 0.0;
            int index = i;
            for (int j=0; j<n; ++j){ //ens recorrem la fila o columna
                partial_sum += (double)input[index]*Math.cos((double)(k*Math.PI*(2.0*j + 1.0))/((double)n*2.0));
                index+=stride;
            }
            double ci;
            if(k==0) ci = 1.0/Math.sqrt(2.0);
            else ci = 1.0;
            output[k] = 0.5*ci*partial_sum;
        }
        return output;
    }


    //Realitza la DCT-2D, no té molt misteri. En desus si es fa la optimitzacio
    private double[] DCT2D(byte [] input){
        int size = input.length;
        double[] output = new double[size];
        double tmp;
        for(int i=0; i<8; ++i){
            for (int j=0; j<8; ++j){
                tmp = 0.0;
                for (int x=0; x<8; ++x){
                    for (int y=0; y<8; ++y){
                        tmp += Math.cos(((2.0*x + 1.0)*Math.PI*i)/(2.0*8.0)) *
                               Math.cos(((2.0*y + 1.0)*Math.PI*j)/(2.0*8.0)) *
                               input[x*8 + y];
                    }
                }
                double ci,cj;
                if(i==0) ci = 1.0/Math.sqrt(2.0);
                else ci = 1.0;
                if (j==0) cj = 1.0/Math.sqrt(2.0);
                else cj = 1.0;
                tmp *= 1.0/Math.sqrt(2.0*8.0) * ci * cj;
                output[i*8 + j] = tmp;
            }
        }
        return output;
    }

    private short[] InverseDCT(short [] input)
    {
        return InverseDCT2D(input);

    }

    //Ńo l'utilitzem.
    private double[] InverseDCT1D( double [] input, int ini, int n, int stride){

        double[] output = new double[n];
        double sum = 0;
        for (int i=0; i<n; ++i){
            sum = 0;
            int index = ini;
            for (int j=0; j<n; ++j){
                double cx;
                if (i==0) cx = 1.0/Math.sqrt(2.0);
                else cx = 1.0;
                sum += cx*input[index]*Math.cos((double)((i*2.0 + 1.0)*Math.PI*j)/(double)n*2.0);
                index += stride;
            }
            output[i] = (int)((sum)/Math.sqrt(1.0*8.0))*1.5;
        }
        return output;
    }


    //DCT-2D inversa
    private short[] InverseDCT2D(short [] input){
        int size = input.length;
        short[] output = new short[size];
        double tmp;

        for(int i=0; i<8; ++i){
            for (int j=0; j<8; ++j){
                tmp = 0.0;
                for (int x=0; x<8; ++x){
                    for (int y=0; y<8; ++y){
                        double cx,cy;
                        if (x==0) cx = 1.0/Math.sqrt(2.0);
                        else cx = 1.0;
                        if (y==0) cy = 1.0/Math.sqrt(2.0);
                        else cy = 1.0;

                        tmp += Math.cos(((2.0*i + 1.0)*Math.PI*x)/(2.0*8.0)) *
                                Math.cos(((2.0*j + 1.0)*Math.PI*y)/(2.0*8.0)) *
                                input[x*8 + y] * cx * cy;
                    }
                };
                output[i*8 + j] = (short)(tmp/Math.sqrt(2.0*8.0));
            }
        }
        return output;
    }

    //Divideix cada element per la matriu de quantització, fent us del ratio
    private short[] Quantization (double [] input, double ratio){
        int size = input.length;
        short[] output = new short[size];
        for (int i=0; i<size; ++i){
            int div = (int)(Qmatrix[i]*bigratio*ratio);
            if(div==0)++div;
            output[i] = (short)(input[i] / div);
        }
        return output;
    }
    //Multiplica cada element per la matriu de quantització, fent us del ratio
    private short[] InverseQuantization (short [] input, double ratio){
        int size = input.length;
        short[] output = new short[size];
        for (int i=0; i<size; ++i){
            int mult = (int)(Qmatrix[i]*bigratio*ratio);
            if (mult==0) ++mult;
            output[i] = (short)(input[i] * mult);
        }
        return output;
    }
    //Reordena l'array en zig-zag, interpretantla com una matriu
    private short[] zigzag(short[] input){
        int n = input.length;
        short[] zz = new short[n];
        int index = 0;
        //Diagonals = num files + num columnes-1
        for(int i=0; i<(8+7); ++i){
            int c = (i<8)?i:7; //Les ultimes "numcolumnes-1" diagonals son totes a la columna 7, les primeres a la i
            int f = (i<8)?0:i-7; //les primeres "numfiles" diagonals comencen a la fila 0, les altres a la i-7
            while(c>=0 && f<8){
                zz[index++] = input[f*8 + c];
                c--;
                f++;
            }
        }
        return zz;
    }
    //fa el procés invers
    private short[] inverse_zigzag(short[] input){
        int n = input.length;
        short[] ret = new short[n];
        int index = 0;
        for(int i=0; i<(8+7);++i){ // i = diagonal number
            int c = (i<8)?i:7; //Les ultimes "numcolumnes-1" diagonals son totes a la columna 7, les primeres a la i
            int f = (i<8)?0:i-7; //les primeres "numfiles" diagonals comencen a la fila 0, les altres a la i-7
            while(c>=0 && f<8){
                ret[f*8 + c] = input[index++];
                ++f;
                --c;
            }
        }
        return ret;
    }

    //canvia els 0 consecutius per 0+num_zeros
    private short[] pack(short[] input){

        int n = input.length;
        ArrayList<Short> packet = new ArrayList<>();
        char count=0;
        boolean start = false;
        for(int i=0; i<n; ++i){
            if (input[i]==0){
                if(!start) {
                    packet.add(input[i]);
                    start=true;
                }
                ++count;
            }
            else{
                if(start){
                    packet.add((short)count);
                    start=false;
                    count=0;
                }
                packet.add(input[i]);
            }
        }
        if(start) packet.add((short)count);
        short[] res = new short[packet.size()];
        for(int i=0; i< packet.size(); ++i) res[i] = packet.get(i);

        return res;
    }

    //canvia 0+num_zero per 00...000
    private short[] inverse_pack(short[] input){

        int n = input.length;
        short res[] = new short[64];
        int index = 0;
        for(int i=0; i<n; ++i){
            res[index++] = input[i];
            if (input[i] == 0){
                i++;
                for(int j=0; j<input[i]-1; ++j) res[index++] = 0;
            }
        }
        return res;
    }



    //Crea la taula de huffman
    private Map<Short,String> CreateHuffman(short [] input){
        //Crea un hashmap amb les dades i les freqüències
        Map<Short, Short> map = new TreeMap<>();
        int size = input.length;
        for(int i=0; i<size; ++i){
            Short freq = map.get(input[i]);
            if (freq == null) {
                map.put(input[i],(short)1);
            }
            else map.put(input[i],(short)(freq+1));
        }
        //Comparador per ordenar el mapa
        Comparator<Short> valueComparator = new Comparator<Short>() {
            @Override
            public int compare(Short k1, Short k2) {
                int compare = map.get(k1).compareTo(map.get(k2));
                if (compare == 0) return 1;
                else return compare;
            }
        };
        //ordena el mapa
        Map<Short,Short> sorted = new TreeMap<>(valueComparator);
        sorted.putAll(map);

        //Crea una arraylist de HuffmanTrees amb el contingut del mapa
        ArrayList<HuffmanTree> top_level = new ArrayList<HuffmanTree>();
        sorted.forEach((k, v) -> top_level.add(new HuffmanTree(k,v)));
        Comparator<HuffmanTree> arbrecomp = new Comparator<HuffmanTree>() {
            @Override
            public int compare(HuffmanTree o1, HuffmanTree o2) {
                if (o2.AconseguirFreq() > o1.AconseguirFreq()) return 1;
                else return 0;
            }
        };
        //Va creant el arbre iterativament, millor explicar al document
        while(top_level.size() > 1){
            HuffmanTree a = top_level.get(0);
            HuffmanTree b = top_level.get(1);
            HuffmanTree newarbre = new HuffmanTree((short)-1,(short)(a.AconseguirFreq() + b.AconseguirFreq()));
            newarbre.AfegirFillEsq(a);
            newarbre.AfegirFillDret(b);
            top_level.remove(a);
            top_level.remove(b);
            top_level.add(newarbre);
            top_level.sort(arbrecomp);
        }
        //Genera el diccionari de manera recursiva
        Map<Short,String> traduction = new TreeMap<>();
        String base = "";
        generateTraduction(top_level.get(0),base,traduction);
        return traduction;
    }
    //funció recursiva a mode de DFS
    void generateTraduction(HuffmanTree current, String code, Map<Short,String> traduction){
        if (current==null){
            return;
        }
        if (!current.teFills()) {
            traduction.put(current.AconseguirNum(),code);
            return;
        }
        generateTraduction(current.AconseguirFillEsq(),code + '0', traduction);
        generateTraduction(current.AconseguirFillDret(), code + "1", traduction);
    }
    //Prepara el contingut per a ser escrit
    private char[] Encode(Map<Short,String> table, short[] input){
        int size = input.length;
        ArrayList<Character> output = new ArrayList<Character>();
        //guarda la taula de huffman
        output.add((char)table.size());
        table.forEach((k, v) -> {
            output.add((char)((k>>8)&0x0ff));
            output.add((char)((k)&0x0ff));
            char stringsize = (char)v.length();
            output.add(stringsize);
            char encoded = 0;
            for(int i=0; i<v.length(); ++i){ //converteix la string a un byte amb uns i zeros
                encoded<<=1;
                if(v.charAt(i)=='1') encoded+=(char)1;
            }
            output.add(encoded);
        });

        //Utilitza la taula per codificar les dades
        int count = 0;
        String actual = "";

        for (int i=0; i<size; ++i){
            String code = table.get(input[i]); //aconsegueix la traducció
            int length = code.length(); //la seva longitut
            if (count + length <= 8) { //mira si cap en el byte actual
                actual += code;
                count += length;
            }else{ //si no cap, posa el que pot en el byte actual i la resta en el següent
                int caben = 8-count; //quants bits caben
                int sobren = length-caben; //quants bits sobren
                for (int j=0; j<caben; ++j) actual += code.charAt(j);
                output.add((char)(Integer.parseInt(actual,2))); //filled, afegit al output. Llegeix un string i el parseja com a binari
                //System.out.printf("%d ",(Integer.parseInt(actual,2)));
                actual = ""; //reinicia actual
                for (int j=0; j<sobren; ++j) actual += code.charAt(caben+j);
                count = sobren;
            }
        }
        //System.out.println();
        if (actual != "") { //si ens ha quedat un byte a mig construir, l'afegim
            for(int i=0; i<8-count; ++i) actual += "0";
            output.add((char) (Integer.parseInt(actual, 2)));
        }
        //afegim un header amb el tamany del canal comprimit, 2 bytes

        int extra_fields=4;
        int totalsize = output.size() + extra_fields;
        char[] ret = new char[totalsize];
        ret[3] = (char)(input.length);
        ret[2] = (char)table.size(); //quantes entrades te la taula
        int tmp = (totalsize) & 0x00FF00;
        ret[0] = (char)((tmp >> 8)&0x0FF); //Size of all huffman data
        ret[1] = (char)((totalsize) & 0x0FF);//Size of all huffman data
        for(int i=4; i<ret.length;++i) ret[i] = output.get(i-4);
        return ret;
    }
    //LLegeix dades comprimides i utilitza la taula de huffman per traduirles
    private short[] Decode(char[] input){
        //mira com de gran es la taula
        int valuesencoded = input[3]&0x0FF;
        int tablesize = (int)(input[2]&0x0FF);
        //Creem un mapa que tradueixi de "traducció (string)" a dada.
        Map<String,Short> table = new TreeMap<>();
        int input_it = 5;
        for(int i=0; i<tablesize; ++i){
            //llegim els dos bytes que representen el numero
            short num = (short)(((input[input_it++]&0x0ff)<<8)+ (input[input_it++]&0x0ff));
            String s = "";
            char size = input[input_it++]; //mirem quants bits ocupa la traducció
            char encode = input[input_it++]; //agafem tot el byte de la traducció
            for (int j=size-1; j>=0; --j){
                if (((encode>>j)&0x01) == 0) s+='0'; //pasem a string els bits necessaris
                else s+='1';
            }
            table.put(s,num);
        }
        //Llegeix i decodifica les dades.
        ArrayList<Short> output = new ArrayList<>();

        int iterador = 0;
        int size = input.length;
        String actual = "";
        byte bit;
        for (int i=input_it; i<size; ++i){ //pel que queda de canal
            char in = input[i]; //llegeix un byte
            for (int j=0; j<8 && iterador<valuesencoded; ++j){ //itera pels bits del byte
                bit = (byte)(in & (1<<(7-j)));
                if(bit!=0){
                    actual += "1";
                }else{
                    actual += "0";
                }
                Short real = table.get(actual); //comprova si existeix la traducció actual, i sinó va llegint bits fins que la trobi
                if (real != null) {
                    //output[iterador++] = real;
                    output.add(real);
                    ++iterador;
                    actual = "";
                }
            }
        }
        short[] res = new short[output.size()];
        for(int i=0; i<output.size(); ++i) res[i] = output.get(i);
        return res;
    }
    //junta els tres canals
    private char[] unifyCompression(char[] a, char[] b, char[] c){
        int size = a.length + b.length + c.length;
        char[] output = new char[size];

        for(int i=0; i<a.length; ++i) output[i]=a[i];
        for(int i=0; i<b.length; ++i) output[i+a.length]=b[i];
        for(int i=0; i<c.length; ++i) output[i+a.length+b.length]=c[i];

        //System.arraycopy(a,0,output,0,a.length);
        //System.arraycopy(b,0,output,a.length,b.length);
        //System.arraycopy(c,0,output,b.length,c.length);
        return output;
    }
    //junta dues strings
    private char[] appendArrays(char[] a, char[] b){
        int size = a.length + b.length;
        char[] output = new char[size];
        for(int i=0; i<a.length; ++i) output[i]=a[i];
        for(int i=0; i<b.length; ++i) output[i+a.length]=b[i];
        return output;
    }

    private char[] appendNArrays(ArrayList<char[]> narrays, int n){
        char[] output = new char[n];
        int na = narrays.size();

        int index = 0;
        for(int i=0; i<na; ++i){
            char[] ci = narrays.get(i);
            int nc = ci.length;
            for(int j=0; j<nc; ++j){
                output[index++] = ci[j];
            }
        }
        return output;
    }

    //aconsegueix el header
    private char[] getHeader(char[] input){
        int lines = 0;
        int offset = 0;
        while (lines < 4) {
            if (input[offset] == '\n') ++lines;
            ++offset;
        }
        char[] header = new char[offset];
        header = Arrays.copyOfRange(input, 0, offset);
        return header;
    }
    //Aconsegueix un canal de un bloc comprimit
    private char[] splitCompression(char[] input, int which){
        int[] sizes = new int[3];
        //mira quan mesuren els canals
        sizes[0] = ((input[0]&0x0ff)<<8) + (input[1]&0x0ff);//&0x0ff;
        sizes[1] = ((input[sizes[0]]&0x0ff) << 8) + (input[sizes[0]+1]&0x0ff);
        sizes[2] = ((input[sizes[0]+sizes[1]]&0x0ff) << 8) + (input[sizes[0]+sizes[1]+1]&0x0ff);

        //System.out.printf("sizes: %d %d %d\n",sizes[0],sizes[1],sizes[2]);

        //Calcula quan comença cada canal
        int[] offsets = {0,sizes[0],sizes[0]+sizes[1]};
        char[] output = new char[sizes[which]]; //-2 bytes de "size of channel" que no seleccionem
        for(int i=0; i<output.length; ++i) output[i] = input[offsets[which]+i];
        return output;

    }

    ///////////////////////  FOR DEBUGGING PURPOSES //////////////////////////////
    public void PrintMatrix(short [] input, int width, int height){
        for(int i=0; i<width*height; ++i){
            System.out.printf("%d ",input[i]);
            if (i%width==width-1) System.out.print("\n");
        }
        System.out.printf("\n---------------\n");
    }
    private void PrintCharMatrix(char [] input, int width, int height){
        for(int i=0; i<width*height; ++i){
            System.out.printf("%d ",(int)input[i]);
            if (i%width==width-1) System.out.print("\n");
        }
        System.out.printf("\n---------------\n");
    }
    private void PrintDoubleMatrix(double [] input, int width, int height){
        for(int i=0; i<width*height; ++i){
            System.out.printf("%.2f ",input[i]);
            if (i%width==width-1) System.out.print("\n");
        }
        System.out.printf("\n---------------\n");
    }
    ////////////////////////////////////////////////////////////////////////////

    public static void main(String args[]){

        short[] lala = {16,11,10,16,24,40,51,61,
                12,12,14,19,26,58,60,55,
                14,13,16,24,40,57,69,56,
                14,17,22,29,51,87,80,62,
                18,22,37,56,68,109,103,77,
                24,35,55,64,81,104,113,0,
                49,64,78,87,103,10,0,0,
                72,92,95,98,10,0,0,0};


        AlgorismeJPEG a = new AlgorismeJPEG();
        a.PrintMatrix(lala,8,8);
        short[] zigzag = a.zigzag(lala);
        System.out.println("zigzag:");
        for(int i=0; i<zigzag.length; ++i) {System.out.print(zigzag[i]); System.out.print(" ");}
        System.out.println();

        short[] packed = a.pack(zigzag);
        System.out.println("packed:");
        for(int i=0; i<packed.length; ++i) {System.out.print(packed[i]); System.out.print(" ");}
        System.out.println();

        Map<Short,String> HuffManTable = a.CreateHuffman(packed);
        char[] encoded = a.Encode(HuffManTable,packed);
        //System.out.println("encoded:");
        //for(int i=0; i<encoded.length; ++i) {System.out.print((int)encoded[i]); System.out.print(" ");}
        //System.out.println();

        short[] decoded = a.Decode(encoded);
        System.out.println("decoded:");
        for(int i=0; i<decoded.length; ++i) {System.out.print((int)decoded[i]); System.out.print(" ");}
        System.out.println();
        System.out.println();
        short[] unpacked = a.inverse_pack(decoded);
        short[] izizag = a.inverse_zigzag(unpacked);
        a.PrintMatrix(izizag,8,8);
    }

}

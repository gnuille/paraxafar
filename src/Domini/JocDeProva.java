package Domini;

import Presentacio.ControladorPresentacio;

import java.util.Scanner;

public class JocDeProva {

    private static ControladorDomini cd;
    private static boolean exit = false;


    private static void descomprimir(){
        System.out.println("Introdueix el path relatiu de l'archiu a descomprimir (Amb extensió)");
        Scanner scanner = new Scanner (System. in);
        String inputString = scanner. nextLine();
        cd.seleccionarFitxerComprimit(inputString);

        System.out.println("Introdueix el nom del archiu descomprimit (Sense extensió)");
        scanner = new Scanner (System. in);
        String inputString2 = scanner. nextLine();
        System.out.println("Descomprimint...pot trigar una estona");
        try {
            cd.descomprimirSeleccionat(inputString2);
        }catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("Descomprimit!");
    }


    private static void comprimir(){
        System.out.println("Introdueix el path relatiu de l'archiu a comprimir (Amb extensió)");
        Scanner scanner = new Scanner (System. in);
        String inputString = scanner. nextLine();
        try {
            cd.seleccionarFitxer(inputString);
        }catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("Introdueix l'algorisme a utilitzar: JPEG, LZ78, LZSS, LZW");
        scanner = new Scanner (System. in);
        String inputString2 = scanner. nextLine();

        if(inputString2.equals("JPEG")){
            System.out.println("selected jpeg");
            System.out.println("Introdueix els tres ratios de compressió [0.0 .. 1.0]: ratioY ratioCb ratioCr");
            scanner = new Scanner (System. in);
            String inputString4 = scanner. nextLine();
            String[] ratios = inputString4.split(" ");
            cd.ratiosJPEG(Double.parseDouble(ratios[0]),Double.parseDouble(ratios[1]),Double.parseDouble(ratios[2]));
        }
        else if(inputString2.equals("LZ78")) System.out.println("selected LZ78");
        else if(inputString2.equals("LZSS")) System.out.println("selected LZSS");
        else if(inputString2.equals("LZW")) System.out.println("selected LZW");

        try {
            if (inputString2.equals("JPEG")) cd.seleccionarAlgorismeManual(TipusAlgorisme.JPEG);
            else if (inputString2.equals("LZ78")) cd.seleccionarAlgorismeManual(TipusAlgorisme.LZ78);
            else if (inputString2.equals("LZSS")) cd.seleccionarAlgorismeManual(TipusAlgorisme.LZSS);
            else if (inputString2.equals("LZW")) cd.seleccionarAlgorismeManual(TipusAlgorisme.LZW);
        }catch(Exception e){
            e.printStackTrace();
        }

        System.out.println("Introdueix el nom del archiu comprimit (sense extensió)");
        scanner = new Scanner (System. in);
        String inputString3 = scanner. nextLine();

        System.out.println("Comprimint...pot trigar una estona");
        try {
            cd.comprimirSeleccionat(inputString3);
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("Comprimit!");
    }

    private static void estadistica() {
        System.out.println("Introdueix el path relatiu de l'arxiu del qual vols saber l'estadística");
        Scanner scanner = new Scanner (System. in);
        String inputString = scanner. nextLine();
        cd.seleccionarFitxerComprimit(inputString);
    }

    private static void inici(){
        System.out.println("Vols comprimir (c), descomprimir (d), veure una estadística (e) o sortir (s)? ");
        Scanner scanner = new Scanner (System. in);
        String inputString = scanner. nextLine();

        if (inputString.equals("c")) comprimir();
        else if (inputString.equals("d")) descomprimir();
        else if (inputString.equals("e")) estadistica();
        else if (inputString.equals("s")) exit = true;
    }

    public static void main(String args[]) {
        cd = new ControladorDomini(new ControladorPresentacio());
        while(!exit) {
            inici();
        }



    }
}

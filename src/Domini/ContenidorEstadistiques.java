package Domini;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * La classe \verb ContenidorEstadistiques té les dues funcions principals per gestionar les estadístiques del programa:
 * per una banda fa de contenidor de les estadístiques concretes generades en els processos de compressió i descompressió,
 * i per l'altra representa les estadístiques globals del nostre programa.  Com que és una classe contenidora l'hem
 * implementat seguint el patró \emph{Singleton}, ja que volem que totes les parts del nostre programa tinguin accés a
 * la mateixa instància de la classe.
 */
public class ContenidorEstadistiques {

    /**
     * Propietats estadistiques generals
      */

    private HashMap<TipusAlgorisme, Double> tempsCompAvg = new HashMap<>();
    private HashMap<TipusAlgorisme, Double> tempsDescompAvg = new HashMap<>();

    private HashMap<TipusAlgorisme, Double> ratioCompressioAvg = new HashMap<>();

    private HashMap<TipusAlgorisme, Integer> nombreCompressions = new HashMap<>();
    private HashMap<TipusAlgorisme, Integer> nombreDescompressions = new HashMap<>();

    /**
     * Contenidor estadistiques
     */
    private ArrayList<EstadisticaConcreta> estadistiques;

    private static ContenidorEstadistiques instance = null;

    private ContenidorEstadistiques() {
        this.estadistiques = new ArrayList<>();

        for (TipusAlgorisme t : TipusAlgorisme.values() ) {
            tempsCompAvg.put(t,0.0);
            tempsDescompAvg.put(t,0.0);
            ratioCompressioAvg.put(t,0.0);
            nombreCompressions.put(t,0);
            nombreDescompressions.put(t,0);
        }
    }

    public static ContenidorEstadistiques obtenirInstancia() {
        if(instance == null) {
            instance = new ContenidorEstadistiques();
        }

        return instance;
    }

    public void estableixLlistaEstadistiques(ArrayList<EstadisticaConcreta> llista) {
        this.estadistiques = llista;
    }

    public void afegeixEstadisticaConcreta(EstadisticaConcreta e) throws Exception {
        this.estadistiques.add(e);
        if(e instanceof EstadisticaConcretaCompressio)      incrementarNombreCompressions(e.aconseguirAlgorismeUtilitzat());
        else if(e instanceof EstadisticaConcretaDescompressio)   incrementarNombreDescompressions(e.aconseguirAlgorismeUtilitzat());
        else throw new Exception();

        recalculaEstadistiquesGlobals(e);
    }

    public EstadisticaConcreta cercaEstadisticaConcreta(int index) {
        return this.estadistiques.get(index);
    }

    public double obtenirTempsMitjaCompressio(TipusAlgorisme algorisme) {
        return this.tempsCompAvg.get(algorisme);
    }

    public double obtenirTempsMitjaDescompressio(TipusAlgorisme algorisme) {
        return this.tempsDescompAvg.get(algorisme);
    }

    public double obtenirRatioCompressio(TipusAlgorisme algorisme) {
        return this.ratioCompressioAvg.get(algorisme);
    }

    public int obtenirNombreCompressions(TipusAlgorisme t) {
        return this.nombreCompressions.get(t);
    }

    public int obtenirNombreDescompressions(TipusAlgorisme t) {
        return this.nombreDescompressions.get(t);
    }

    public void establirNombreCompressions(TipusAlgorisme t, Integer n) {
        this.nombreCompressions.put(t, n);
    }

    public void establirNombreDescompressions(TipusAlgorisme t, Integer n) {
        this.nombreDescompressions.put(t, n);
    }

    public void establirEstadistiquesGlobals(double[][] valors) {
        for (int i = 0; i < 4; i++) {
            this.ratioCompressioAvg.put(TipusAlgorisme.values()[i], valors[i][0]);
            this.tempsCompAvg.put(TipusAlgorisme.values()[i], valors[i][1]);
            this.tempsDescompAvg.put(TipusAlgorisme.values()[i], valors[i][2]);
        }
    }

    private void incrementarNombreCompressions(TipusAlgorisme t) {
        this.nombreCompressions.put(t, nombreCompressions.get(t) + 1);
    }

    private void incrementarNombreDescompressions(TipusAlgorisme t) {
        this.nombreDescompressions.put(t, nombreDescompressions.get(t) + 1);
    }

    private void recalculaEstadistiquesGlobals(EstadisticaConcreta nova) {

        if(nova instanceof EstadisticaConcretaCompressio) {
            double nouValorCompTemps = ((tempsCompAvg.get(nova.aconseguirAlgorismeUtilitzat())
                    * (nombreCompressions.get(nova.aconseguirAlgorismeUtilitzat())-1))
                    + nova.aconsegueixTemps())
                    / (nombreCompressions.get(nova.aconseguirAlgorismeUtilitzat()));

            double nouValorRatio = ((ratioCompressioAvg.get(nova.aconseguirAlgorismeUtilitzat())
                    * (nombreCompressions.get(nova.aconseguirAlgorismeUtilitzat())-1))
                    + ((EstadisticaConcretaCompressio) nova).aconsegueixRatio())
                    / (nombreCompressions.get(nova.aconseguirAlgorismeUtilitzat()));

            tempsCompAvg.put(nova.aconseguirAlgorismeUtilitzat(), nouValorCompTemps);
            ratioCompressioAvg.put(nova.aconseguirAlgorismeUtilitzat(), nouValorRatio);
        } else {
            double nouValorDescompTemps = ((tempsDescompAvg.get(nova.aconseguirAlgorismeUtilitzat())
                    * (nombreDescompressions.get(nova.aconseguirAlgorismeUtilitzat())-1))
                    + nova.aconsegueixTemps())
                    / (nombreDescompressions.get(nova.aconseguirAlgorismeUtilitzat()));
            tempsDescompAvg.put(nova.aconseguirAlgorismeUtilitzat(), nouValorDescompTemps);
        }

    }

}

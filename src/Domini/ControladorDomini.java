/**
 * @author
 */

package Domini;

import Persistencia.ControladorPersistencia;
import Presentacio.ControladorPresentacio;

import javax.naming.ldap.Control;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Consumer;

import static java.lang.System.exit;

public class ControladorDomini {

  private TipusAlgorisme algorismeSeleccionat;

  private Algorisme algorisme;
  private Fitxer fitxerSeleccionat;
  private FitxerComprimit fitxerComprimitSeleccionat;

  private ContenidorEstadistiques est;

  private double ratioY,ratioCb,ratioCr;
  private int numthreads;

  private ControladorPresentacio cp;
  private ControladorPersistencia cper;


  public ControladorDomini(ControladorPresentacio cp) {
    this.numthreads = 1;
    this.cp = cp;
    this.cper = new ControladorPersistencia();
    this.carregaEstadistiques();
  }

  private String extreureExtencio(String s) {
    int inici_extencio = s.length() - 1;
    while (inici_extencio != -1) {
      if (s.charAt(inici_extencio) == '.') break;
      inici_extencio--;
    }
    String extencio = s.substring(inici_extencio + 1);
    return extencio;
  }

  private String descaratrXafar(String s) {
    int inici_extencio = s.length() - 1;
    while (inici_extencio != -1) {
      if (s.charAt(inici_extencio) == '.') break;
      inici_extencio--;
    }
    String extencio = s.substring(inici_extencio);
    if (!extencio.equals(".xafar")) return s;
    return s.substring(0, inici_extencio);
  }

  public void seleccionarFitxer(String nom) throws Exception {
    String extencio = extreureExtencio(nom);

    switch (extencio) {
      case "txt":
        this.fitxerSeleccionat = new Text("", nom);
        break;
      case "ppm":
        this.fitxerSeleccionat = new Imatge("", nom);
        break;
      default:
        throw new Exception();
    }
  }

  public void seleccionarFitxerComprimit(String nom) {
    this.fitxerComprimitSeleccionat = new FitxerComprimit("", nom);
  }

  public void seleccionarAlgorismeManual(TipusAlgorisme algorisme) throws Exception {
    this.algorismeSeleccionat = algorisme;
  }

  public void seleccionarAlgorismeAutomatica() throws Exception {
    String nom = this.fitxerSeleccionat.aconseguirCami();
    String extencio = extreureExtencio(nom);

    switch (extencio) {
      case "txt":
        this.algorismeSeleccionat = TipusAlgorisme.LZW;
      case "jpeg":
        this.algorismeSeleccionat = TipusAlgorisme.JPEG;
      default:
        throw new Exception();
    }
  }

  public void comprimirConjunt(String nom_comp, String[] paths, String[] algorismes, double[] ratios) throws Exception {
    if (paths.length != algorismes.length) throw new Exception();
    int index_ratio = 0;
    EstadisticaConcretaCompressio e;

    cper.obrirFitxerComprimit(nom_comp);
    int count = 0;
    int nFiles = paths.length;

    String base_name = descaratrXafar(nom_comp);

    WorkerPool wp = new WorkerPool(numthreads);

    double average_compress = 0.0;
    double total_time = 0.0;

    for (int i = 0; i < nFiles; i+=numthreads) {
      //mirem si necesitem menys threads
      int actualthreads = numthreads;
      if (nFiles - i < numthreads) actualthreads = nFiles - i;
      //repartim la feina
      for(int w=0; w<actualthreads; ++w){
        //si es jpeg, agafem els ratios
        double[] possible_ratios = new double[3];
        if (algorismes[i+w].equals("JPEG")) possible_ratios = new double[]{ratios[index_ratio++],ratios[index_ratio++],ratios[index_ratio++]};
        //creem els threads
        ArrayList<Object> params = new ArrayList<>(Arrays.asList(paths[i+w],algorismes[i+w],possible_ratios));
        wp.DoWork(this::comprimirConjunt_thread,params);
      }
      //esperem a que acabin
      wp.WaitAll();
      //recollim els resultats
      for(int w=0; w<actualthreads; ++w){
        ArrayList<Object> result = (ArrayList<Object>)wp.getWork(w);
        char[] contingut_comp = (char[])result.get(0);
        EstadisticaConcretaCompressio ecc = (EstadisticaConcretaCompressio)result.get(1);
        System.out.println(ecc.aconsegueixRatio());
        average_compress += ecc.aconsegueixRatio();
        total_time += ecc.aconsegueixTemps();
        cper.afegirComprimit(base_name + "/" +  paths[i+w], algorismes[i+w], contingut_comp);
      }
      count += actualthreads;
      cp.processStatus(((count * 100) / nFiles));
      wp.Clear();
    }
    //retornem estadistiques de tota la compressio
    cp.retornEstadistiques(average_compress/nFiles, total_time);
    cper.escriureComprimit();
    cp.processStatus(100);
  }

  public ArrayList<Object> comprimirConjunt_thread(ArrayList<Object> params) throws Exception{

    String path = (String)params.get(0);
    String alg = (String)params.get(1);
    double[] ratios = (double[])params.get(2);

    char[] contingut_orig = cper.obrir_i_llegir(path);

    TipusAlgorisme algorisme_seleccionat_thread;
    Algorisme algorisme_thread;
    Fitxer fitxer_seleccionat_thread;

    if (alg.equals("JPEG")) {
      algorisme_seleccionat_thread = TipusAlgorisme.JPEG;
      algorisme_thread = new AlgorismeJPEG(ratios[0], ratios[1], ratios[2], 1);
      fitxer_seleccionat_thread = new Imatge("", path);
    } else if (alg.equals("LZW")) {
      algorisme_seleccionat_thread = TipusAlgorisme.LZW;
      algorisme_thread = new AlgorismeLZW();
      fitxer_seleccionat_thread = new Text("", path);
    } else if (alg.equals("LZSS")) {
      algorisme_seleccionat_thread = TipusAlgorisme.LZSS;
      algorisme_thread = new AlgorismeLZSS();
      fitxer_seleccionat_thread = new Text("", path);
    } else if (alg.equals("LZ78")) {
      algorisme_seleccionat_thread = TipusAlgorisme.LZ78;
      algorisme_thread = new AlgorismeLZ78();
      fitxer_seleccionat_thread = new Text("", path);
    } else throw new Exception();

    fitxer_seleccionat_thread.establirContingut(contingut_orig);

    double t_inici = System.currentTimeMillis();

    algorisme_thread.comprimir(fitxer_seleccionat_thread);
    char[] contingut_comp = algorisme_thread.aconseguirContingut();

    double t_final = System.currentTimeMillis();
    EstadisticaConcretaCompressio e = new EstadisticaConcretaCompressio(t_final - t_inici, algorisme_seleccionat_thread, contingut_orig.length, contingut_comp.length);
    est.afegeixEstadisticaConcreta(e);

    ArrayList<Object> output = new ArrayList<>();
    output.add(contingut_comp);
    output.add(e);
    return output;

  }


  public void comprimirSeleccionat(String nom) throws Exception {

    EstadisticaConcretaCompressio e;
    cper.obrirFitxer(this.fitxerSeleccionat.aconseguirCami());
    char[] contingut_orig = cper.llegirFitxer();
    this.fitxerSeleccionat.establirContingut(contingut_orig);
    String id_algorisme = "";

    cper.obrirFitxerComprimit(nom);
    switch (this.algorismeSeleccionat) {
      case JPEG:
        algorisme = new AlgorismeJPEG(ratioY, ratioCb, ratioCr,numthreads);
        id_algorisme = "JPEG";
        break;
      case LZ78:
        algorisme = new AlgorismeLZ78();
        id_algorisme = "LZ78";
        break;
      case LZSS:
        algorisme = new AlgorismeLZSS();
        id_algorisme = "LZSS";
        break;
      case LZW:
        algorisme = new AlgorismeLZW();
        id_algorisme = "LZW";
        break;
      default:
        throw new Exception();
    }

    double t_inici = System.currentTimeMillis();

    this.algorisme.comprimir(this.fitxerSeleccionat);
    char[] contingut_comp = this.algorisme.aconseguirContingut();
    cper.afegirComprimit(this.fitxerSeleccionat.aconseguirCami(), id_algorisme, contingut_comp);
    cper.escriureComprimit();

    double t_final = System.currentTimeMillis();
    e = new EstadisticaConcretaCompressio(t_final - t_inici, algorismeSeleccionat, contingut_orig.length, contingut_comp.length);
    est.afegeixEstadisticaConcreta(e);
    cp.retornEstadistiques(e.aconsegueixRatio(), e.aconsegueixTemps());
    cp.processStatus(100);
  }

  public void descomprimirSeleccionat(String nom) throws Exception {
    EstadisticaConcretaDescompressio e = null;
    int count = 0;
    cper.obrirFitxerComprimit(this.fitxerComprimitSeleccionat.aconseguirCami());

    cper.llegirComprimit();
    int nFitxers = cper.nombreFitxers();
    double t_ini = System.currentTimeMillis();
    while (cper.quedenFitxers()) {

      count++;
      this.fitxerComprimitSeleccionat.establirContingut(cper.aconseguirContingut());
      Fitxer descomprimit = null;
      TipusAlgorisme tipusAlgorisme = null;

      String nom_actual = cper.aconseguirNom();

      switch (cper.aconseguirAlgorisme()) {
        case "JPEG":
          descomprimit = new Imatge("", nom + ".ppm");
          algorisme = new AlgorismeJPEG(numthreads);
          tipusAlgorisme = TipusAlgorisme.JPEG;
          break;
        case "LZ78":
          descomprimit = new Text("", nom + ".txt");
          algorisme = new AlgorismeLZ78();
          tipusAlgorisme = TipusAlgorisme.LZ78;
          break;
        case "LZSS":
          descomprimit = new Text("", nom + ".txt");
          algorisme = new AlgorismeLZSS();
          tipusAlgorisme = TipusAlgorisme.LZSS;
          break;
        case "LZW":
          descomprimit = new Text("", nom + ".txt");
          algorisme = new AlgorismeLZW();
          tipusAlgorisme = TipusAlgorisme.LZW;
          break;
        default:
          throw new Exception();
      }

      if (nFitxers > 1) {
        descomprimit.establirCami(nom_actual);
      }

      double t_inici = System.currentTimeMillis();
      this.algorisme.descomprimir(this.fitxerComprimitSeleccionat);
      double t_final = System.currentTimeMillis();
      cper.obrirFitxer(descomprimit.aconseguirCami());
      if (nFitxers > 1) cper.crearDirecotris(descomprimit.aconseguirCami());

      cper.escriureFitxer(this.algorisme.aconseguirContingut());

      e = new EstadisticaConcretaDescompressio(t_final - t_inici, tipusAlgorisme);
      est.afegeixEstadisticaConcreta(e);
      cper.seguentFitxer();
      cp.processStatus(((count * 100)/ nFitxers));
    }

    if (e == null) throw new Exception();
    cp.processStatus(100);
    if (count == 1)
      cp.retornEstadistiques(-1, e.aconsegueixTemps());
    else
      cp.retornEstadistiques(-1,System.currentTimeMillis()-t_ini);
  }




  public void ratiosJPEG(double a, double b, double c) {
    ratioY = a;
    ratioCb = b;
    ratioCr = c;
  }

  public void setnumthreads(int n){
    numthreads = n;
    if (numthreads > 128) numthreads = 128;
  }


  private void carregaEstadistiques() {
    this.est = ContenidorEstadistiques.obtenirInstancia();
    //est.estableixLlistaEstadistiques(); // Posem el que obtenim de la capa de persistencia
  }

  public double[][] retornaEstadistiquesGlobals() {
    double[][] ret = new double[4][3]; //4 algorismes, dos estadistiques per algorisme


    ret[0][0] = est.obtenirRatioCompressio(TipusAlgorisme.JPEG);
    ret[0][1] = est.obtenirTempsMitjaCompressio(TipusAlgorisme.JPEG)/1000.0;
    ret[0][2] = est.obtenirTempsMitjaDescompressio(TipusAlgorisme.JPEG)/1000.0;

    ret[1][0] = est.obtenirRatioCompressio(TipusAlgorisme.LZW);
    ret[1][1] = est.obtenirTempsMitjaCompressio(TipusAlgorisme.LZW)/1000.0;
    ret[1][2] = est.obtenirTempsMitjaDescompressio(TipusAlgorisme.LZW)/1000.0;

    ret[2][0] = est.obtenirRatioCompressio(TipusAlgorisme.LZ78);
    ret[2][1] = est.obtenirTempsMitjaCompressio(TipusAlgorisme.LZ78)/1000.0;
    ret[2][2] = est.obtenirTempsMitjaDescompressio(TipusAlgorisme.LZ78)/1000.0;

    ret[3][0] = est.obtenirRatioCompressio(TipusAlgorisme.LZSS);
    ret[3][1] = est.obtenirTempsMitjaCompressio(TipusAlgorisme.LZSS)/1000.0;
    ret[3][2] = est.obtenirTempsMitjaDescompressio(TipusAlgorisme.LZSS)/1000.0;

    return ret;
  }

  public BufferedImage retornaBufferedImage(String path, boolean comprimit){
    Imatge im = new Imatge("", ".tmp_img.ppm");
    try {
      if (comprimit) {
        cper.obrirFitxerComprimit(path);
        cper.llegirComprimit();
        if (cper.nombreFitxers() != 1) {
          System.err.println("Nombre fitxers no es 1, es: " + cper.nombreFitxers());
          throw new Exception();
        }

        FitxerComprimit fct = new FitxerComprimit(" ", ".tmp_img.xafar");
        AlgorismeJPEG algorismeJPEG = new AlgorismeJPEG(numthreads);
        fct.establirContingut(cper.aconseguirContingut());


        algorismeJPEG.descomprimir(fct);
        im.establirContingut(algorismeJPEG.aconseguirContingut());
      } else {
        cper.obrirFitxer(path);
        im.establirContingut(cper.llegirFitxer());
      }
    }catch (Exception e){
      e.printStackTrace();
    }

    int width = im.aconseguirAmplada();
    int height = im.aconseguirAltura();
    char[] contingut = im.aconseguirData();
    BufferedImage bi = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
    //BufferedImage bi = new BufferedImage(height,width,BufferedImage.TYPE_INT_RGB);
    for(int i=0; i<height; i++){
      for(int j=0; j<width; j++){
        char R = contingut[i*width*3 + j*3];
        char G = contingut[i*width*3 + j*3 + 1];
        char B = contingut[i*width*3 + j*3 + 2];
        bi.setRGB(j,i,(R<<16) + (G<<8) + B);
      }
    }

    return bi;
  }

  public String retornaBufferedRead(String path, boolean comprimit){
    Text t = new Text("", ".tmp_txt.txt");
    String ret = null;
    try {

      if (comprimit) {
        cper.obrirFitxerComprimit(path);
        cper.llegirComprimit();
        if (cper.nombreFitxers() != 1) {
          System.err.println("Nombre fitxers no es 1, es: " + cper.nombreFitxers());
          throw new Exception();
        }

        Algorisme algorisme = null;
        FitxerComprimit fct = new FitxerComprimit(" ", ".tmp_txt.xafar");

        switch (cper.aconseguirAlgorisme()) {
          case "LZW":
            System.err.println("IN LZW");
            algorisme = new AlgorismeLZW();
            break;
          case "LZ78":
            algorisme = new AlgorismeLZ78();
            break;
          case "LZSS":
            algorisme = new AlgorismeLZSS();
            break;
          default:throw new Exception();
        }

        fct.establirContingut(cper.aconseguirContingut());

        algorisme.descomprimir(fct);
        t.establirContingut(algorisme.aconseguirContingut());
      } else {
        cper.obrirFitxer(path);
        t.establirContingut(cper.llegirFitxer());
      }


      //if(comprimit) {
      //  t = new Text("lala","asdf1234.txt");
      //  seleccionarFitxerComprimit(path);
      //  fitxerComprimitSeleccionat.Llegir();
      //  algorismeSeleccionat = fitxerComprimitSeleccionat.extreureAlgorisme();

      //  switch (this.algorismeSeleccionat) {
      //    case JPEG:
      //    case LZ78:
      //      algorisme = new AlgorismeLZ78(); break;
      //    case LZSS:
      //      algorisme = new AlgorismeLZSS(); break;
      //    case LZW:
      //      algorisme  = new AlgorismeLZW();  break;
      //    default: break;
      //  }
      //  algorisme.descomprimir(fitxerComprimitSeleccionat);
      //  t.afegirContingut(algorisme.aconseguirContingut());
      //  t.Escriure();
      //  path = "asdf1234.txt";
      //}
      ret = String.valueOf(t.aconseguirContingut());
      System.out.println(ret);
    }catch (Exception e){
      e.printStackTrace();
    }
    //if(comprimit) {
    //  File f = new File(path);
    //  f.delete();
    //}
    return ret;
  }
/*
  public static void test(char[] a, char[] b){
    try {
      Thread.currentThread().sleep(1000);
    }catch(InterruptedException e){
      e.printStackTrace();
    }
    System.out.println("aaa");
    //return new char[1];
  }
*/

  public static void main(String args[]) {


    /*
    try {
      ControladorDomini c = new ControladorDomini();
      c.seleccionarFitxer("lorem.txt");
      c.seleccionarAlgorismeManual(TipusAlgorisme.LZW);
      c.comprimirSeleccionat("lorem");

      c.seleccionarFitxerComprimit("lorem.xafar");
      c.descomprimirSeleccionat("lorem.2");
    } catch (Exception e) {
      e.printStackTrace();
    }
    */

    try {
      ControladorDomini c = new ControladorDomini(new ControladorPresentacio());
      /*char[] jaja = new char[2];
      WorkerPool wp = new WorkerPool(4);
      //myFunction<char[], char[]> f = test();
      wp.DoWork(ControladorDomini::test,jaja,jaja);
      wp.DoWork(ControladorDomini::test,jaja,jaja);
      wp.DoWork(ControladorDomini::test,jaja,jaja);
      wp.DoWork(ControladorDomini::test,jaja,jaja);
      wp.DoWork(ControladorDomini::test,jaja,jaja);*/


      c.seleccionarFitxer("dvd.ppm");
      c.seleccionarAlgorismeManual(TipusAlgorisme.JPEG);
      c.ratiosJPEG(0.2,0.9,0.9);
      c.comprimirSeleccionat("dvd");

      //c.seleccionarFitxerComprimit("vincent.xafar");
      //c.descomprimirSeleccionat("vincent_d");
    } catch (Exception e) {
      e.printStackTrace();
    }

  }



}

/**
 * @author Pablo Vizcaino
 * @author Joan Vinyals
 */

package Domini;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

abstract class Fitxer {

    private String nom;
    protected String path;
    protected char[] contingut;

    Fitxer(String nom, String path) {
        this.nom  = nom;
        this.path = path;
    }

    public void afegirContingut(char[] contingut){
        this.contingut = contingut;
    }

    public char[] aconseguirContingut() {
       return contingut;
    }

    public void establirContingut(char[] contingut) {
        this.contingut = contingut;
    }

    public void Escriure(){
        File newfile = new File(path);
        try {
            FileOutputStream stream = new FileOutputStream(newfile);
            byte[] towrite = new byte[contingut.length];
            for(int i=0; i<towrite.length;++i){
                towrite[i] = (byte)((int)contingut[i]);
            }
            stream.write(towrite);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public void Llegir(){
        File f = new File(path);
        try {
            byte[] byte_contents = Files.readAllBytes(f.toPath());
            contingut = new char[byte_contents.length];
            for(int i=0; i< byte_contents.length; ++i) contingut[i] = (char)byte_contents[i];
        }catch(IOException e) {
            e.printStackTrace();
        }
    }

    public String aconseguirCami() {
        return this.path;
    }

    public void establirCami(String cami){
        this.path = cami;
    }
}

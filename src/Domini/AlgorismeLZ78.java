/**
 * @author Marc Clasca
 */

package Domini;

import Domini.util.ArbreLZW;

import java.util.*;

class AlgorismeLZ78 extends Algorisme {

    // Inicialitzem l'arbre que ens farà de diccionari
    ArbreLZW<Character, Integer> D = new ArbreLZW<>(null, 0);

    /**
     * Número d'entrades màximes que pot tenir el diccionari.
     * Depen de la mida amb la qual es codifica l'index en la compressió:
     *  1b => 255, 2b => 65535, etc
     */
    private static int MAX_DICC_RANG = 255;

    protected void comprimir(Fitxer f) {
        char[] cadena = f.aconseguirContingut();

        Integer w = 0;
        ArrayList<Character> cadenaCodificada = new ArrayList(); // codificara la w en 2 bytes, per tant MAX(w) = 65 535
        ArbreLZW<Character, Integer> a = D;
        int indexAssignats = 0;

        for (int i = 0; i < cadena.length; i++) {

            char k = cadena[i];
            // Mirem si tenim el caracter k als fills de l'arbre
            if(D.teFills()) {
                ArbreLZW<Character, Integer> aux = D.aconseguirFill(k);
                if(aux != null) {
                    // Tenim cadena, avancem
                    w = aux.aconseguirValor();
                    D = aux;
                    continue;
                }
            }


            cadenaCodificada.add((char)w.intValue());
            cadenaCodificada.add((k));
            // TOken Codificat: [(char), (char)] size: 4 bytes
            //                   ^enter w

            D.afegirFill(k, indexAssignats+1);
            indexAssignats++;
            w = 0;
            D = a;


            if(indexAssignats>=MAX_DICC_RANG) {
                indexAssignats = 0;
                D = new ArbreLZW<>(null, 0);
                a = D;
            }
        }

        this.contingut = new char[cadenaCodificada.size()];
        for(int i = 0; i < cadenaCodificada.size(); i++) {
            this.contingut[i] = cadenaCodificada.get(i);
        }


        return;

    }

    protected void descomprimir(FitxerComprimit fc) {
        char[] cadena = fc.aconseguirContingut(); // Carreguem continguts en format cadena de bytes

        String descodificat = new String();
        Map<Integer, String> Dict = new HashMap<>();
        int indexAssignat = 0;


        for (int i = 0 ; i < cadena.length; i+=2) {
            // Obtenim els dos bytes de w

            char charValor = cadena[i];
            int w = charValor;
            w = w & 0x00ff;

            // Obtenim char
            char k = cadena[i+1];

            String sw = "";
            if(w != 0) {
                sw = Dict.get(w);
            }
            descodificat += sw;
            descodificat += k;

            Dict.put(++indexAssignat, sw+k);


            if(indexAssignat>=MAX_DICC_RANG) {
                indexAssignat = 0;
                Dict = new HashMap<>();
            }

        }

        this.contingut = descodificat.toCharArray();
    }


}



/**
 * @author Joan Vinyals
 */

package Domini;

import Domini.util.ArbreLZW;
import Domini.util.CadenaDeBits;

import java.util.Hashtable;
import java.util.Vector;

class AlgorismeLZW extends Algorisme {

    private Fitxer          fitxer;
    private FitxerComprimit fitxerComprimit;

    private CadenaDeBits      comprimit;
    private Vector<Character> descomprimit;

    private static final int max_index = (1 << 13) - 1;

    AlgorismeLZW() { }

    public void comprimir (Fitxer fitxer) {
        this.fitxer = fitxer;
        char[] caracters = this.fitxer.aconseguirContingut();
        Vector<Character> vector_caracters = new Vector<>();
        for (int i = 0; i < caracters.length; i++) {
            vector_caracters.add(caracters[i]);
        }
        this.descomprimit = vector_caracters;
        comprimir_lzw();
        super.contingut = this.comprimit.aconseguirCharArray();
    }

    public void descomprimir (FitxerComprimit fitxerComprimit) {
        this.fitxerComprimit = fitxerComprimit;

        this.comprimit = this.fitxerComprimit.aconseguirCadenaDeBits();
        descomprimir_lzw();
        super.contingut = new char[this.descomprimit.size()];
        for (int i = 0; i < this.descomprimit.size(); i++) {
            super.contingut[i] = this.descomprimit.get(i);
        }
    }

    private void comprimir_lzw () {
        this.comprimit = new CadenaDeBits();

        ArbreLZW<Character, Integer> arbreDeClaus = new ArbreLZW<>(null, 0);
        for (char c = 1; c < 256; c++) {
            arbreDeClaus.afegirFill(c, (int) c);
        }
        Integer ultim_index = 256;

        ArbreLZW<Character, Integer> arbreActual = arbreDeClaus;
        for (Character character : this.descomprimit) {
            ArbreLZW<Character, Integer> arbreAuxiliar = arbreActual.aconseguirFill(character);
            if (arbreAuxiliar == null) {
                arbreActual.afegirFill(character, ultim_index);
                ultim_index++;
                if (ultim_index > max_index) {
                    arbreDeClaus = new ArbreLZW<>(null, 0);
                    for (char c = 1; c < 256; c++) {
                        arbreDeClaus.afegirFill(c, (int) c);
                    }
                    ultim_index = 256;
                }
                Integer valor = arbreActual.aconseguirValor();
                arbreActual = arbreDeClaus.aconseguirFill(character);
                this.comprimit.afegirBits(valor, 12);
            } else {
                arbreActual = arbreAuxiliar;
            }
        }
        this.comprimit.afegirBits(arbreActual.aconseguirValor(), 12);
    }

    private void descomprimir_lzw () {
        this.descomprimit = new Vector<>();
        Hashtable<Integer, Vector<Character>> taulaDeResums = new Hashtable<>();
        for (char c = 1; c < 256; c++) {
            Vector<Character> vector_unic = new Vector<>();
            vector_unic.add(c);
            taulaDeResums.put((int) c, vector_unic);
        }

        int ultim_index = 256;
        Vector<Integer> comprimitEnters = this.comprimit.aconseguirIntVector(12);

        Vector<Character> ultimSegment = null;
        for (Integer current : comprimitEnters) {
            if (current == 0) {
                this.descomprimit.add((char) 0);
            }
            Vector<Character> segmentDescomprimit = taulaDeResums.get(current);
            if (segmentDescomprimit != null) {
                segmentDescomprimit = new Vector<>(segmentDescomprimit);
                this.descomprimit.addAll(segmentDescomprimit);

                if (ultimSegment != null) {
                    ultimSegment.add(segmentDescomprimit.get(0));
                    taulaDeResums.put(ultim_index, new Vector<>(ultimSegment));
                    ultim_index++;
                    if (ultim_index > max_index) {
                        taulaDeResums = new Hashtable<>();
                        for (char c = 1; c < 256; c++) {
                            Vector<Character> vector_unic = new Vector<>();
                            vector_unic.add(c);
                            taulaDeResums.put((int) c, vector_unic);
                        }
                        ultim_index = 256;
                    }
                }
                ultimSegment = segmentDescomprimit;
            } else {
                if (ultimSegment != null) {
                    ultimSegment.add(ultimSegment.get(0));
                    this.descomprimit.addAll(ultimSegment);
                    taulaDeResums.put(ultim_index, new Vector<>(ultimSegment));
                    ultim_index++;
                    if (ultim_index > max_index) {
                        taulaDeResums = new Hashtable<>();
                        for (char c = 0; c < 256; c++) {
                            Vector<Character> vector_unic = new Vector<>();
                            vector_unic.add(c);
                            taulaDeResums.put((int) c, vector_unic);
                        }
                        ultim_index = 256;
                    }
                }
            }
        }
    }

    public CadenaDeBits aconseguirCadenaComprimida() {
        return this.comprimit;
    }

}

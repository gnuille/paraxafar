/**
 * @author Marc Clasca
 */

package Domini;

/**
 * Aquesta subclasse no implementa cap mètode o propietat més enllà dels que ja hereta de \verb EstadisticaConcreta .
 */
public class EstadisticaConcretaDescompressio extends EstadisticaConcreta {

    public EstadisticaConcretaDescompressio(double temps, TipusAlgorisme a) {
        super(temps, a);
    }
}

/**
 * @author Joan Vinyals
 */

package Domini;

import java.io.File;
import java.util.ArrayList;

public class TextStub extends Text {

    private ArrayList<Character> contingut;

    protected TextStub(String nom, String path) {
        super(nom, path);
    }

    public TextStub() {
        super("TextStub", "");
    }

    @Override
    public char[] aconseguirContingut() {
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Malesuada proin libero nunc consequat interdum varius. Varius morbi enim nunc faucibus a pellentesque sit. Lectus mauris ultrices eros in cursus turpis massa tincidunt dui. Sapien et ligula ullamcorper malesuada. A pellentesque sit amet porttitor eget dolor morbi. Justo laoreet sit amet cursus sit amet. Venenatis cras sed felis eget velit. Aliquam sem et tortor consequat id porta. Lectus sit amet est placerat. Ultrices neque ornare aenean euismod elementum nisi. Viverra justo nec ultrices dui. Mattis nunc sed blandit libero. Consequat nisl vel pretium lectus quam id. Luctus accumsan tortor posuere ac ut. Ultrices vitae auctor eu augue ut lectus arcu bibendum. Vitae tortor condimentum lacinia quis vel eros. Nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Orci phasellus egestas tellus rutrum. Facilisis magna etiam tempor orci eu lobortis elementum nibh.".toCharArray();
    }

}

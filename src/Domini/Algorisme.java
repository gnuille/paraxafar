/**
 * @author Joan Vinyals
 */

package Domini;

abstract class Algorisme {

    protected char[] contingut;
    protected abstract void comprimir(Fitxer fitxer);
    public char[] aconseguirContingut() {return contingut;}
    protected abstract void descomprimir(FitxerComprimit fitxerComprimit);

}

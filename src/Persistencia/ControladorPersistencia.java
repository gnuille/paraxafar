/**
 * @author Joan Vinyals
 */
package Persistencia;

import Domini.ControladorDomini;
import Persistencia.Exception.ExcepcioEscripturaPersistencia;
import Persistencia.Exception.ExcepcioLecturaPersistencia;

import java.io.File;

/**
 * Classe: ControladorPersistencia
 * ==============================
 * Es el controlador de la capa de persistencia, conte totes les funcions necessaries necesaries per utilitzar aquesta
 * capa.
 */
public class ControladorPersistencia {

    // Fitxer que s'esta tractant actualment.
    private FitxerPersistent fp;

    // Fitxer comprimit que s'esta tractant actualment.
    private FitxerComprimitPersistent fcp;

    private byte[] contingutAEscriure;

    /**
     * Funcio: ControladorPersistencia
     * ---------------------------------
     * Es la constructora d'aquesta classe, no pren res com a parametre.
     *
     * @return nou objecta ControladorPersistencia
     */
    public ControladorPersistencia() { }

    /**
     * Funcio: obrirFitxer
     * ---------------------
     * Crea un nou FitxerPersistent, el guarda a l'attribut privat fp de la classe.
     *
     * @param cami del sistema de fitxers, on es troba o trobarà el fitxer.
     */
    public void obrirFitxer(String cami) {
        this.fp = new FitxerPersistent(cami);
    }

    /**
     * Funció: obrirFitxerComprimit
     * ----------------------------
     * Crea un nou FitxerComprimitPersistent, el guarda a l'attribut privat fcp de la classe.
     *
     * @param cami del sitema de fitxers, on es troba o trobarà el fitxer comprimit.
     */
    public void obrirFitxerComprimit(String cami) {
        this.fcp = new FitxerComprimitPersistent(cami);
    }

    /**
     * Funcio: llegirFitxer
     * --------------------
     * Carrega a memoria el contingut del fitxer fp i en retorna el contingut.
     *
     * @return el contingut de fp.
     * @throws ExcepcioLecturaPersistencia
     */
    public char[] llegirFitxer() throws ExcepcioLecturaPersistencia {
        this.fp.llegir();
        return this.fp.aconseguirContingut();
    }

    public char[] obrir_i_llegir(String cami) throws ExcepcioLecturaPersistencia{

        FitxerPersistent fper = new FitxerPersistent(cami);
        fper.llegir();
        return fper.aconseguirContingut();
        /*
        obrirFitxer(cami);
        return llegirFitxer();
         */
    }
    
    /**
     * Funcio: llegirComprimit
     * -----------------------
     * Carrega a memoria el contingut del fitxer comprimit fcp.
     *
     * @throws Exception en cas d'error en la lectura.
     */

    public void llegirComprimit() throws Exception {
        this.fcp.llegir();
    }

    /**
     * Funcio: escriureFitxer
     * ----------------------
     * Escriu a disc contingut.
     *
     * @param contingut
     * @throws ExcepcioEscripturaPersistencia en cas d'error en la escriptura.
     */
    public void escriureFitxer(char[] contingut) throws ExcepcioEscripturaPersistencia {
        this.fp.establirContingut(contingut);
        this.fp.escriure();
    }

    /**
     * Funcio: seguentFitxer
     * ---------------------
     * Descarta el fitxer de dalt de la pila, i prepara el seguent per ser tractat.
     *
     * @throws Exception en cas de que no quedin mes fitxers.
     */
    public void seguentFitxer() throws Exception {
        this.fcp.saltarAlSeguentFitxer();
    }

    /**
     * Funcio: quedenFitxers
     * ---------------------
     * Comproba si queden fitxers dins de fcp.
     *
     * @return cert en cas que quedin fitxers, fals altrement.
     * @throws Exception en cas d'error.
     */
    public boolean quedenFitxers() throws Exception {
        return this.fcp.quedenFitxers();
    }

    /**
     * Funcio: nombreFitxers
     * ---------------------
     * Dona el nombre de fitxers que queden per tractar dins de fcp.
     *
     * @return el nombre de fitxers que queden.
     */
    public int nombreFitxers() {
        return this.fcp.nombreFitxers();
    }

    /**
     * Funcio: aconseguirNom
     * ---------------------
     * Aconsegueix el nom del fitxer de dalt de la pila de fcp.
     *
     * @return el nom.
     * @throws Exception
     */
    public String aconseguirNom() throws Exception {
        return this.fcp.aconseguirNom();
    }

    /**
     * Funcio: aconseguirAlgorisme
     * ---------------------------
     * Aconsegueix l'algorisme del fitxer a dal de la pila dins de fcp.
     *
     * @return retorna un String que representa un identificador de l'algorisme que s'ha fet servir per comprimit
     * el fitxer en questio.
     * @throws Exception
     */
    public String aconseguirAlgorisme() throws Exception {
        switch (this.fcp.aconseguirAlgorisme()) {
            case 0:
                return "LZW";
            case 1:
                return "LZSS";
            case 2:
                return "LZ78";
            case 3:
                return "JPEG";
            default: throw new Exception();
        }
    }

    /**
     * Funcio: aconseguirContingut
     * ---------------------------
     * Demana el contingut a fcp el contingut del primer fitxer comprimit dins d'aques FitxerComprimitPersistent.
     *
     * @return retorn a el contingut del fitxer contingut.
     * @throws Exception
     */
    public char[] aconseguirContingut() throws Exception{
        return this.fcp.aconseguirContingut();
    }

    /**
     * Funcio: afegirComprimit
     * -----------------------
     * Afegeix un fitxer dins el fcp, gurdant'ne el nom (original) el tipus d'algorisme que s'ha fet servir
     * (id_algorisme) i el contingut (comprimit) del fitxer.
     *
     * @param original
     * @param id_algorisme
     * @param contingut
     * @throws Exception en cas de rebre un id_algorisme invalid.
     */
    public void afegirComprimit(String original, String id_algorisme, char[] contingut) throws Exception {
        byte algorisme = 0;
        switch (id_algorisme) {
            case "LZW": algorisme = 0; break;
            case "LZSS": algorisme = 1; break;
            case "LZ78": algorisme = 2; break;
            case "JPEG": algorisme = 3; break;
            default: throw new Exception();
        }
        this.fcp.afegirFitxerComprimit(original, algorisme, contingut);
    }

    /**
     * Funcio: escriureComprimit
     * -------------------------
     * Escriu al sistema de fitxers el contingut guradat per fcp.
     *
     * @throws ExcepcioEscripturaPersistencia en cas de falla en la expriptura.
     */
    public void escriureComprimit() throws ExcepcioEscripturaPersistencia {
        this.fcp.escriure();
    }

    /**
     * Funcio: extreureCarpeta
     * -----------------------https://repo.hca.bsc.es/gitlab/EPI/RTL/Vector_Accelerator
     * Retorna la el cami a la carpeta on es troba el fitxer amb cami s.
     *
     * @param s
     * @return un String que representa la jerarqui de carpetes on es troba el fitxer amb cami s.
     */
    private String extreureCapeta(String s) {
        int inici_extencio = s.length() - 1;
        while (inici_extencio != -1) {
            if (s.charAt(inici_extencio) == '/') break;
            inici_extencio--;
        }
        String extencio = s.substring(0, inici_extencio);
        return extencio;
    }

    /**
     * Funcio: crearDirectorio
     * -----------------------
     * Donada un String cami que indica el cami de un fitxeer crea les carpetes (si es necessari) on es troba el fitxer
     * en qüestio.
     *
     * @param cami
     */
    public void crearDirecotris(String cami) {
        File carpetaBuilder = new File(extreureCapeta(cami));
        carpetaBuilder.mkdirs();
    }
}

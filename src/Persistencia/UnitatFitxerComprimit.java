package Persistencia;

/**
 * Classe: UnitatFitxerComprimit
 * =============================
 * Aquesta estroctura de dades conte la informacio d'un unic fitxer comprimit dins la multitud de fitxers que pot
 * contenir un fitxer .xafar.
 */
public class UnitatFitxerComprimit {

    // Nom del fitxer original.
    private String nom;

    // Tipus d'algorisme que es va utilitzar per comprimit el fitxer.
    private byte alorisme;

    // Contingut comprimit del fitxer.
    private char[] contingut;

    // Contingut de disc, amb les capçalers que contenen els camps anteriors ( nom, algorisme i contingut ).
    private byte[] contingut_cru;

    /**
     * Funcio: UnitatFitxerComprimit
     * -----------------------------
     * Crea una nova unitat de fitxer comprimit desmuntat, a patir dels parametres que se li passen.
     *
     * @param original nom del fitxer abans de comprimit.
     * @param alorisme utilitzar per comprimit el contingut.
     * @param contingut contingut comprimit del fitxer.
     */
    public UnitatFitxerComprimit(String original, byte alorisme, char[] contingut) {
        this.nom = original;
        this.alorisme = alorisme;
        this.contingut = contingut;
    }

    /**
     * Funcio: construirUnitat
     * -----------------------
     * Munta la UnitatFitxerComprimit, dona una cadena de bytes que conte tant el contingut del fitxer com les
     * metadades.
     *
     * @return el contingut a escriure a disc.
     */
    public byte[] construirUnitat() {
        int size = ( this.nom.length() + 1 ) + 1 + this.contingut.length + (3 * (this.contingut.length / 0x00ffffff)) + (this.contingut.length % 0x00ffffff == 0 ? 0 : 3)  + 3;
        this.contingut_cru = new byte[size];
        int i;
        for (i = 0; i < nom.length(); i++) {
            this.contingut_cru[i] = (byte) nom.charAt(i);
        }
        this.contingut_cru[i++] = (byte) 0;
        this.contingut_cru[i++] = this.alorisme;

        int content_size = this.contingut.length;
        int current_posi = 0;

        int bsize; // Bloc size
        do {
            int left = content_size - current_posi;
            bsize = left > 0x00ffffff ? 0x00ffffff : left;

            this.contingut_cru[i++] = (byte) ((0x00ff0000 & bsize) >> 16);
            this.contingut_cru[i++] = (byte) ((0x0000ff00 & bsize) >> 8);
            this.contingut_cru[i++] = (byte) ((0x000000ff & bsize) >> 0);

            for (int j = 0; j < bsize; j++) {
                this.contingut_cru[i++] = (byte) (0x00ff & this.contingut[current_posi++]);
            }
        } while (bsize > 0);

        return this.contingut_cru;
    }

    /**
     * Funcio: aconseguirNom
     * ---------------------
     * @return el nom del fitxer.
     */
    public String aconseguirNom() {
        return this.nom;
    }

    /**
     * Funcio: aconseguirAlgorisme
     * ---------------------------
     * @return el codi associat a l'algorisme.
     */
    public byte aconseguirAlorisme() {
        return this.alorisme;
    }

    /**
     * Funcio: aconseguirContingut
     * ---------------------------
     * @return el contingut comprimit del fitxer.
     */
    public char[] aconseguitContingut() {
        return this.contingut;
    }
};

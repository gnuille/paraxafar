/**
 * @author Joan Vinyals
 */
package Persistencia;

import Persistencia.Exception.ExcepcioEscripturaPersistencia;
import Persistencia.Exception.ExcepcioLecturaPersistencia;

public interface ObjectePersistent {


    char[] aconseguirContingut() throws Exception;
    void establirContingut(char[] contingut);

    void escriure() throws ExcepcioEscripturaPersistencia;
    void llegir()   throws ExcepcioLecturaPersistencia;

}

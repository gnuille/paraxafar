/**
 * @author Joan Vinyals
 */

package Persistencia;

import Persistencia.Exception.ExcepcioEscripturaPersistencia;
import Persistencia.Exception.ExcepcioLecturaPersistencia;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FitxerPersistent implements ObjectePersistent {

    private String cami;
    private char[] contingut;

    public FitxerPersistent(String cami) {
        this.cami = cami;
    }

    @Override
    public char[] aconseguirContingut() {
        return this.contingut;
    }

    @Override
    public void establirContingut(char[] contingut) {
        this.contingut = contingut;
    }

    @Override
    public void escriure() throws ExcepcioEscripturaPersistencia {
       try {
           FileOutputStream stream = new FileOutputStream(this.cami, false);
           byte[] toWrite = new byte[this.contingut.length];
           for (int i = 0; i < this.contingut.length; i++) {
               toWrite[i] = (byte) (0x00ff & this.contingut[i]);
           }
           stream.write(toWrite);
           stream.close();
       } catch (Exception e) {
           e.printStackTrace();
           throw new ExcepcioEscripturaPersistencia();
       }
    }

    @Override
    public void llegir() throws ExcepcioLecturaPersistencia {
        try {
            FileInputStream stream = new FileInputStream(this.cami);
            int size = stream.available();
            this.contingut = new char[size];
            for (int i = 0; i < size; i++) {
                this.contingut[i] = (char) stream.read();
            }
            stream.close();
        }catch(IOException e) {
            throw new ExcepcioLecturaPersistencia();
        }
    }

}

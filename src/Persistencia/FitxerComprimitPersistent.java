/**
 * @author Joan Vinyals
 */
package Persistencia;

import Persistencia.Exception.ExcepcioEscripturaPersistencia;
import Persistencia.Exception.ExcepcioLecturaPersistencia;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

/**
 * Classe: FitxerComprimitPersistent
 * =================================
 * Estroctura de dades que conte tots els mecanismes necessaris per tant escriure com llegir fitxers comprimits.
 * Donat un fitxer comprimit que pot contenir diversos fitxers dins, aquesta classe o bé monta, o bé desmonta, el fitxer comprimit,
 * a partir de les seves capçalerses en diverses UnitatFitxerComprimit.
 */
public class FitxerComprimitPersistent implements ObjectePersistent {

    // Cami on es guarda o es llegeix el fitxer
    private String cami;

    // Contingut desmuntat en un array de UnitatFitxerComprimit.
    private ArrayList<UnitatFitxerComprimit> contingut;

    /**
     * Funcio: nombreFitxers
     * ---------------------
     * La mida de contingut.
     * @return el nombre de fitxers que conte.
     */
    public int nombreFitxers() {
        return this.contingut.size();
    }

    /**
     * Funcio: FitxerComprimitPersitent
     * --------------------------------
     * Crea una intancia de FitxerComprimitPersistent amb la ubicacio establerta per cami.
     * @param cami
     */
    public FitxerComprimitPersistent(String cami) {
        this.cami = cami;
        this.contingut = new ArrayList<>();
    }

    /**
     * Funcio: afegirFitxerComprimit
     * -----------------------------
     * Crea una nova UnitatFitxerComprimit a partir dels parametres nom, algorisme i contingut.
     *
     * @param nom
     * @param algorisme
     * @param contingut
     */
    public void afegirFitxerComprimit(String nom, byte algorisme, char[] contingut) {
        UnitatFitxerComprimit nou = new UnitatFitxerComprimit(nom, algorisme, contingut);
        this.contingut.add(nou);
    }

    /**
     * Funcio: quedenFitxers
     * ---------------------
     * Comproba si la llista contingut esta buida.
     * @return cert si contingut te elements, altrement retorna fals.
     */
    public boolean quedenFitxers() {
        return !this.contingut.isEmpty();
    }

    /**
     * Funcio: saltarAlSeguentFitxer
     * -----------------------------
     * Elimina el primer element de contingut.
     * @throws Exception si contingut estava buit.
     */
    public void saltarAlSeguentFitxer() throws Exception {
        if (this.contingut.isEmpty()) throw new Exception();
        this.contingut.remove(0);
    }

    /**
     * Funcio: aconseguirNom
     * ---------------------
     * Extreu el nom del primer element de contingut.
     *
     * @return el nom del primer element de contingut.
     * @throws Exception si contingut esta buit.
     */
    public String aconseguirNom() throws Exception {
        if (this.contingut.isEmpty()) throw new Exception();
        return this.contingut.get(0).aconseguirNom();
    }

    /**
     * Funcio: aconseguirAlgorisme
     * ---------------------------
     * Extreu l'algorisme utilitzat per comprimir el primer fitxer de comprimit.
     *
     * @return l'identificadot del primer element de contingut.
     * @throws Exception si contingut esta buit.
     */
    public byte aconseguirAlgorisme() throws Exception {
        if (this.contingut.isEmpty()) throw new Exception();
       return this.contingut.get(0).aconseguirAlorisme();
    }

    /**
     * Funcio: aconseguirContingut
     * ---------------------------
     * Extreu el contingut (cadena de bytes) del primer element de contingut.
     *
     * @return el contingut en un char[]
     * @throws Exception si contingut es buit.
     */
    @Override
    public char[] aconseguirContingut() throws Exception {
        if (this.contingut.isEmpty()) throw new Exception();
        return this.contingut.get(0).aconseguitContingut();
    }

    /**
     * Funcio: establirConingut
     * ------------------------
     * Aquesta funcio no esta implementada, ja que no te sentit. Pel mecanisme que s'utilitza.
     *
     * @param contingut
     */
    @Override
    public void establirContingut(char[] contingut) { return; }

    /**
     * Funcio: escriure
     * ----------------
     * Itera sobre tots els elements de contingut i escriu el contingut de cada element construit.
     *
     * @throws ExcepcioEscripturaPersistencia si hi ha algun error en la escriptura.
     */
    @Override
    public void escriure() throws ExcepcioEscripturaPersistencia {
        try {
            FileOutputStream stream = new FileOutputStream(this.cami, false);
            for (UnitatFitxerComprimit u : this.contingut) {
                stream.write(u.construirUnitat());
            }
            this.contingut = new ArrayList<>();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExcepcioEscripturaPersistencia();
        }
    }

    /**
     * Funcio: llegir
     * --------------
     * Llegeix el contingut de dist hi va creant les UnitatFitxerComprimit a partir de les capçaleres.
     *
     * @throws ExcepcioLecturaPersistencia en cas d'error de l'ectura.
     */
    @Override
    public void llegir() throws ExcepcioLecturaPersistencia {
        try {
            int index = 0;
            FileInputStream stream = new FileInputStream(this.cami);
            int size = stream.available();
            do {

                String nom = "";
                for (int i = 0; i < size; i++) {
                    char current = (char) stream.read();
                    if (current == 0) break;
                    nom += current;
                }
                byte algorisme = (byte) stream.read();
                String contingut = "";
                int mida;
                do {
                    mida = 0;
                    mida <<= 8;
                    mida |=  (0x0ff & stream.read());
                    mida <<= 8;
                    mida |=  (0x0ff & stream.read());
                    mida <<= 8;
                    mida |=  (0x0ff & stream.read());

                    char[] contingutByte = new char[mida];
                    for (int i = 0; i < mida; i++) {
                        contingutByte[i] = (char) stream.read();
                    }
                   contingut += String.valueOf(contingutByte);
                } while (mida != 0);

                System.err.println(nom);
                UnitatFitxerComprimit nou = new UnitatFitxerComprimit(nom, algorisme, contingut.toCharArray());
                this.contingut.add(nou);
            } while ((size = stream.available()) > 0);

        }catch(IOException e) {
            e.printStackTrace();
            throw new ExcepcioLecturaPersistencia();
        }
    }

   void llegir_rapid() throws ExcepcioLecturaPersistencia {
       try {

           RandomAccessFile file = new RandomAccessFile(this.cami, "r");
           long size = file.length();
           int index = 0;
           do {
               int mida_total = 0;
               ArrayList<byte[]> content = new ArrayList<>();
               /* Extracting name for the current file */
               String nom = "";
               for (int i = 0; i < size; i++) {
                   char current = file.readChar(); index++;
                   if (current == 0) break;
                   nom += current;
               }

               /* Extracting algorithm */
               byte algorisme = file.readByte(); index++;

               /* Iterating until mida is 0 */
               int last_mida = 0;
               do {
                   /* Extraient mida de la capçalera */
                   int mida = 0;
                   mida <<= 8;
                   mida |=  (0x0ff & file.readByte()); index++;
                   mida <<= 8;
                   mida |=  (0x0ff & file.readByte()); index++;
                   mida <<= 8;
                   mida |=  (0x0ff & file.readByte()); index++;
                   last_mida = mida;

                   /* Extreient el contingut */
                   byte[] contingut_byte = new byte[mida];
                   file.seek(index);
                   file.read(contingut_byte, 0, mida); index += mida;
                   content.add(contingut_byte);
                   mida_total += mida;
               } while (last_mida != 0);

               char[] contingut = new char[mida_total];
               int j = 0;
               for (byte[] a : content) {
                   for (int i = 0; i < a.length; i++) {
                       contingut[j++] = (char) a[i];
                   }
               }

               System.err.println(nom);
               UnitatFitxerComprimit nou = new UnitatFitxerComprimit(nom, algorisme, contingut);
               this.contingut.add(nou);
           } while ((size = (file.length() - index)) > 0);
       }catch(IOException e) {
           e.printStackTrace();
           throw new ExcepcioLecturaPersistencia();
       }
   }

    private void BuilderUnitatFitxerComprimit(RandomAccessFile file) {
    }

}

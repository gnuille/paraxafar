/**
 * @author pvizcaino
 */
package Presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class myPopup{

    private int ret=0;

    public myPopup(int x, int y, String msg, JFrame frame,boolean cancancel) {
        //frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //default: si es tenca, fer exit.
        //pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        //pane.add(d,c);

        JDialog d = new JDialog(frame);
        d.setLocationRelativeTo(frame);
        d.setModal(true);
        d.setLayout(new GridBagLayout());
        d.setSize(x,y);


        JLabel label = new JLabel(msg);
        //label.setSize(x-30,y/2);
        c.gridx=0;
        c.gridy=0;
        //c.ipadx=x/2;
        c.insets= new Insets(10,10,10,10);
        //c.weightx=1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        if(cancancel) c.gridwidth=2;
        //c.fill=GridBagConstraints.CENTER;
        d.add(label,c);

        c.ipadx=0;
        c.fill = GridBagConstraints.CENTER;
        c.gridwidth=1;
        c.weightx=1.0;

        JButton exitbutton = new JButton("Okay");
        exitbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println("okay");
                ret=0;
                d.dispose();
            }
        });
        c.gridx = 0;
        c.gridy = 1;
        c.insets= new Insets(0,50,0,0);
        d.add(exitbutton,c);


        if(cancancel){
            JButton cancelbutton = new JButton("Cancel");
            cancelbutton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    System.out.println("cancel");
                    ret=1;
                    d.dispose();
                }
            });
            c.gridx = 1;
            c.gridy = 1;
            c.insets= new Insets(0,0,0,50);
            d.add(cancelbutton,c);
        }

        d.setVisible(true);

    }

    public int getRet(){
        return ret;
    }


}

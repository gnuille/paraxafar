package Presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class numThreads {

    private JTextField numthreads;
    private String numthreads_string;

    public numThreads(int x, int y, JFrame frame) {
        GridBagConstraints c = new GridBagConstraints();
        JDialog d = new JDialog(frame);
        d.setLocationRelativeTo(frame);
        d.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //default: si es tenca, no fer res. Cal posar valors!!
        d.setModal(true);
        d.setLayout(new GridBagLayout());
        d.setSize(x,y);

        //Parella de Label i TextField

        JLabel numthreads_txt = new JLabel("Number of threads: ");
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(0,30,0,0);
        d.add(numthreads_txt, c);

        numthreads = new JTextField();
        c.gridx = 1;
        c.weightx = 1;
        c.insets = new Insets(0,5,0,50);
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        d.add(numthreads, c);
        c.weightx=0;

        JButton Ok = new JButton("Okay");
        Ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //Es comprova si s'han introduit els 3 ratios
                if (numthreads.getText().equals("") ){
                    myPopup mp = new myPopup(320,120,"<html>Introdueix un numero de threads abans de continuar</html>",frame,false);
                }else if (numthreads.getText().equals("0")) {
                    myPopup mp = new myPopup(320, 120, "<html>0 no és un numero de threads vàlid</html>", frame, false);
                }else{
                    numthreads_string = numthreads.getText();
                    d.dispose();
                }
            }
        });
        c.gridx=0;
        c.gridy=1;
        c.gridwidth=2;
        c.anchor = GridBagConstraints.SOUTH;
        c.fill = GridBagConstraints.CENTER;
        d.add(Ok,c);
        d.setVisible(true);
    }

    public String getNumThreads(){ return numthreads_string;}

}

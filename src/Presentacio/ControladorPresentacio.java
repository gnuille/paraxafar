/**
 * @author pvizcaino
 */

package Presentacio;

import Domini.ControladorDomini;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.util.ArrayList;


public class ControladorPresentacio {

    private Missatges m;
    private Thread interlocutor;
    private MainWindow mw;
    private Process p;


    public ControladorPresentacio(){
        m = Missatges.getInstance();
        interlocutor = new Thread(new InterlocutorAmbDomini(this));
        interlocutor.start();
        mw = new MainWindow(700,480, this);
        mw.show();
    }
    //enviar missatge per a que el thread de ControladorDomini comprimeixi un fitxer
    public void compress(String pathinicial, String algorisme, String pathfinal){
        m.msg_pathini=pathinicial; m.msg_pathfinal=pathfinal; m.msg_algorisme=algorisme;
        m.msg_compress=true;
        if(algorisme.equals("JPEG")) p = new Process(300,150,this,"Comprimint...",1,pathinicial,pathfinal);
        else p = new Process(300,150,this,"Comprimint...",2,pathinicial,pathfinal);
        p.center(mw.getFrame());
        p.show();

    }
    //enviar missatge per a que el thread de ControladorDomini descomprimeixi un fitxer
    public void decompress(String pathinicial, String pathfinal){
        m.msg_pathini=pathinicial; m.msg_pathfinal=pathfinal;
        m.msg_decompress=true;
        p = new Process(300,150,this,"Decomprimint...",0,null,null);
        p.show();
    }
    //enviar missatge per a que el thread de ControladorDomini canvii els ratios de compressió del JPEG
    public void setRatios(double a, double b, double c){
        m.msg_a=a;m.msg_b=b;m.msg_c=c;
        m.msg_ratios=true;
    }
    //enviar missatge amb numthreads
    public void setnumthreads(int a){
        m.msg_numthreads_n=a;
        m.msg_numthreads=true;
    }

    //Canvia el percentatge del procés de compressió que es mostra a l'usuari
    public void processStatus(int percent){
        if(p!=null){
            p.UpdateProcess(percent);
            if(percent == 100) {
                p.Completed();
            }
        }
    }

    //enviar missatge per a que el thread de ControladorDomini es mori
    public void killThread(){
        m.killthread = true;
    }

    //retorn d'estadistiques al comprimir / descomprimir. ratio és -1 si es una descompressió
    public void retornEstadistiques(double ratio, double temps){
        if(p!=null){
            p.estableixEstadistiques(ratio,temps);
        }
    }

    //peticio de estadistiques globals
    public double[][] demanaEstadistiquesGlobals(){
        m.msg_eg = true;
        while(m.msg_estglob == null){
            try {
                Thread.currentThread().sleep(50);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            //System.out.print("");
        }
        double[][] ret = m.msg_estglob;
        m.msg_estglob = null;
        return ret;
    }

    public BufferedImage demanaBufferedImage(String path, boolean comprimit){
        m.msg_buff_path = path;
        m.msg_buff_c = comprimit;
        m.msg_bImg = true;
        while(m.msg_buffImg == null){
            try {
                Thread.currentThread().sleep(100);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        BufferedImage ret = m.msg_buffImg;
        m.msg_buffImg = null;
        return ret;
    }
    public String demanaBufferedReader(String path, boolean comprimit){
        m.msg_buff_path = path;
        m.msg_buff_c = comprimit;
        m.msg_bRead = true;
        while(m.msg_buffRead == null){
            try {
                Thread.currentThread().sleep(100);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        String ret = m.msg_buffRead;
        m.msg_buffRead = null;
        return ret;
    }

    public void comprimirConjunt(String pathf, ArrayList<String> paths, ArrayList<String> algorismes, ArrayList<Double> ratios){
        String[] pathsarray = new String[paths.size()];
        for(int i=0; i<paths.size(); ++i) pathsarray[i] = paths.get(i);
        String[] algorismesarray = new String[algorismes.size()];
        for(int i=0; i<paths.size(); ++i) algorismesarray[i] = algorismes.get(i);
        double[] ratiosarray = new double[ratios.size()];
        for(int i=0; i<paths.size() && i < ratios.size(); ++i) ratiosarray[i] = ratios.get(i);

        m.msg_pathiniCjt=pathsarray;
        m.msg_algorismesCjt=algorismesarray;
        m.msg_ratiosCjt=ratiosarray;
        m.msg_pathfinal=pathf;
        m.msg_compressconj=true;

        p = new Process(300,150,this,"Comprimint...",0,null,null);
        p.show();
    }



    //Punt d'entrada del programa
    public static void main(String args[]) {
        ControladorPresentacio cp = new ControladorPresentacio();
    }


}

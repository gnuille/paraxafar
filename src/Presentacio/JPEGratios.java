/**
 * @author pvizcaino
 */
package Presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//Popup que demana els ratios de compressió a l'usuari
public class JPEGratios {

    private JTextField ratioY_t,ratioCb_t,ratioCr_t;
    private String Y,Cb,Cr;

    public JPEGratios(int x, int y, JFrame frame) {
        GridBagConstraints c = new GridBagConstraints();
        JDialog d = new JDialog(frame);
        d.setLocationRelativeTo(frame);
        d.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //default: si es tenca, no fer res. Cal posar valors!!
        d.setModal(true);
        d.setLayout(new GridBagLayout());
        d.setSize(x,y);

        //Parelles de Labels i TextFields

        JLabel ratioY = new JLabel("ratioY: ");
        c.gridx = 0;
        c.gridy = 0;
        d.add(ratioY, c);

        ratioY_t = new JTextField();
        c.gridx = 1;
        c.weightx = 1;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        d.add(ratioY_t, c);
        c.weightx=0;

        JLabel ratioCb = new JLabel("ratioCb: ");
        c.gridx = 2;
        c.gridy = 0;
        d.add(ratioCb, c);

        ratioCb_t = new JTextField();
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        d.add(ratioCb_t, c);
        c.weightx = 0;

        JLabel ratioCr = new JLabel("ratioCr: ");
        c.gridx = 4;
        c.gridy = 0;
        d.add(ratioCr, c);

        ratioCr_t = new JTextField();
        c.gridx = 5;
        c.gridy = 0;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        d.add(ratioCr_t, c);
        c.weightx = 0;

        JButton Ok = new JButton("Okay");
        Ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                //Es comprova si s'han introduit els 3 ratios
                if (ratioY_t.getText().equals("") || ratioCb_t.getText().equals("") ||ratioCr_t.getText().equals("")){
                    myPopup mp = new myPopup(320,120,"<html>Introdueix tots els ratios abans de continuar:<br/> Aconsellem: 0.2 0.9 0.9</html>",frame,false);
                }else {
                    Y=(ratioY_t.getText());
                    Cb=(ratioCb_t.getText());
                    Cr=(ratioCr_t.getText());
                    d.dispose();
                }
            }
        });
        c.gridx=0;
        c.gridy=1;
        c.gridwidth=6;
        c.anchor = GridBagConstraints.SOUTH;
        c.fill = GridBagConstraints.CENTER;
        d.add(Ok,c);
        d.setVisible(true);
    }

    public String getY(){return Y;}
    public String getCb(){return Cb;}
    public String getCr(){return Cr;}
}

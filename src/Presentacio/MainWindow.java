/**
 * @author pvizcaino
 */

package Presentacio;

import Domini.TipusAlgorisme;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.util.ArrayList;

//classe principal del controlador de presentació.
public class MainWindow extends Window {

    private JLabel pathfinal;
    private JButton button_i_c,button_i_d,button_clear,button_o,button_c,button_d, button_est, button_exit;
    private PanellFitxers pf;
    private boolean contains_compressed;

    public MainWindow(int x, int y, ControladorPresentacio cp){
        super(x,y,cp);

        pane.setBackground(new Color(50,50,50));

        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        //////// ELEMENT #0 : PANELL DE FITXERS SELECCIONAT /////////
        pf = new PanellFitxers(x-50,(y*4)/6);

        c.gridx=0;
        c.gridy=0;
        c.weightx=1.0;
        c.weighty=1.0;
        c.gridwidth=4;
        pane.add(pf.getScroll(),c);

        c.gridwidth=1;
        c.weightx=0;
        c.weighty=0;


        ///////// ELEMENT #1 : BOTO SELECCIONAR FITXER ///////////////
        button_i_c = new JButton("Seleccionar Fitxer");
        button_i_c.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PressedButton_i_c();
            }
        });
        button_i_c.setBackground(new Color(240,240,240));
        c.gridx=0;
        c.gridy=1;
        c.insets = new Insets(0,10,0,0);
        c.fill=GridBagConstraints.HORIZONTAL;
        pane.add(button_i_c,c);
        c.insets = new Insets(0,0,0,0);


        ///////// ELEMENT #2 : BOTO SELECCIONAR COMPRIMIT ///////////////
        button_i_d = new JButton("Seleccionar Comprimit");
        button_i_d.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PressedButton_i_d();
            }
        });
        button_i_d.setBackground(new Color(240,240,240));
        c.gridx=1;
        c.gridy=1;
        c.fill=GridBagConstraints.HORIZONTAL;
        pane.add(button_i_d,c);


        ///////// ELEMENT #3 : BOTO SELECCIONAR SORTIDA ///////////////
        button_o = new JButton("Seleccionar Sortida");
        button_o.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PressedButton_o();
            }
        });
        button_o.setBackground(new Color(240,240,240));
        button_o.setEnabled(false);
        c.gridx = 2;
        c.gridy = 1;
        c.fill=GridBagConstraints.HORIZONTAL;
        pane.add(button_o,c);

        ///////// ELEMENT #4 : BOTO NETEJAR ///////////////
        button_clear = new JButton("Netejar");
        button_clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PressedButton_clear();
            }
        });
        button_clear.setBackground(new Color(240,240,240));
        c.gridx=3;
        c.gridy=1;
        c.insets = new Insets(0,0,0,10);
        c.fill=GridBagConstraints.HORIZONTAL;
        pane.add(button_clear,c);
        c.insets = new Insets(0,0,0,0);


        ///////// ELEMENT #5 : BOTO COMPRIMIR ///////////////
        button_c = new JButton("Comprimeix");
        button_c.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PressedButton_c(actionEvent);
            }
        });
        button_c.setBackground(new Color(240,240,240));
        button_c.setEnabled(false);
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth=2;
        c.insets = new Insets(10,0,10,0);
        //c.fill = GridBagConstraints.HORIZONTAL;
        c.fill = GridBagConstraints.CENTER;
        pane.add(button_c,c);
        c.gridwidth=0;


        ///////// ELEMENT #6 : BOTO DESCOMPRIMIR ///////////////
        button_d = new JButton("Descomprimeix");
        button_d.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PressedButton_d(actionEvent);
            }
        });
        button_d.setBackground(new Color(240,240,240));
        button_d.setEnabled(false);
        c.gridx = 2;
        c.gridy = 2;
        c.gridwidth=2;
        c.insets = new Insets(10,0,10,0);
        pane.add(button_d,c);
        c.gridwidth=1;
        c.insets = new Insets(0,0,0,0);


        ///////// ELEMENT #7 : TEXT SELECCIONAR SORTIDA ///////////////
        JLabel sortida = new JLabel("SORTIDA:");
        sortida.setForeground(new Color(10,10,10));
        c.gridx = 1;
        c.gridy = 3;
        c.fill = GridBagConstraints.CENTER;
        c.gridwidth = 2;
        pane.add(sortida,c);


        ///////// ELEMENT #8 : PATH FINAL ///////////////
        pathfinal = new JLabel("Cap path de sortida seleccionat");
        pathfinal.setForeground(new Color(200,40,10));
        c.gridx = 1;
        c.gridy = 4;
        c.fill = GridBagConstraints.CENTER;
        pane.add(pathfinal,c);
        c.gridwidth = 1;

        ///////// ELEMENT #9 : BOTO ESTADISTIQUES GLOBALS ///////////////
        button_est = new JButton("Estadístiques globals");
        button_est.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PressedButton_est();
            }
        });
        button_est.setBackground(new Color(240,240,240));
        //button_est.setEnabled(false);
        c.gridx = 0;
        c.gridy = 5;
        c.anchor = GridBagConstraints.LAST_LINE_START;
        pane.add(button_est,c);

        ///////// ELEMENT #10 : BOTO SELECCIONAR COMPRIMIT ///////////////
        button_exit = new JButton("Exit");
        button_exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PressedButton_exit();
            }
        });
        button_exit.setBackground(new Color(240,240,240));
        c.gridx = 3;
        c.gridy = 5;
        c.anchor = GridBagConstraints.LAST_LINE_END;
        pane.add(button_exit,c);

    }

    private void PressedButton_est(){


        double[][] est_d = cp.demanaEstadistiquesGlobals();
        String[][] est = new String[4][3];
        for(int i=0; i<4; ++i){
            for(int j=0; j<3; ++j){
                if(j==0)est[i][j] = String.format("%.2f",est_d[i][j]);
                else est[i][j] = String.format("%.3f",est_d[i][j]);

            }
        }


        String msg = "<html> <b>JPEG:</b> ratio mitjà=" + est[0][0] + "  |  temps mitjà comp=" + est[0][1] + " seconds " + "  |  temps mitjà desc=" + est[0][2] + " seconds <br/><br/>" +
                     "<html> <b>LZW:</b> ratio mitjà=" + est[1][0] + "   |  temps mitjà comp=" + est[1][1] + " seconds " + "  |  temps mitjà desc=" + est[0][2] + " seconds <br/><br/>" +
                     "<html> <b>LZ78:</b> ratio mitjà=" + est[2][0] + "  |  temps mitjà comp=" + est[2][1] + " seconds " + "  |  temps mitjà desc=" + est[0][2] + " seconds <br/><br/>" +
                     "<html> <b>LZSS:</b> ratio mitjà=" + est[3][0] + "  |  temps mitjà comp=" + est[3][1] + " seconds " + "  |  temps mitjà desc=" + est[0][2] + " seconds </html>";
        myPopup est_glob = new myPopup(700,200,msg,frame,false);

    }
    private void PressedButton_exit(){
        cp.killThread();
        frame.dispose();
        System.exit(0);
    }

    private void PressedButton_i_c(){
        int ret=0;
        if(contains_compressed && pf.getnum()>0) {
            myPopup mp = new myPopup(350,120,"<html>Tens fitxers comprimits seleccionats.<br/> Aquesta acció els deseleccionarà. Vols seguir?</html>",frame,true);
            ret = mp.getRet();
            if(ret==0) pf.ClearPanell();
            else return;
        }
        contains_compressed = false;
        button_o.setEnabled(true);
        SelectorFitxer sf = new SelectorFitxer(Accio.LOAD);

        pf.UpdatePanell(sf.getFiletree(), sf.getFiles(),frame);
        if (pf.getnum() > 0 && !pathfinal.getText().equals("Cap path de sortida seleccionat")) {
            button_c.setEnabled(true);
            button_d.setEnabled(false);
        }
    }

    private void PressedButton_i_d(){
        //popup si hi ha fitxers de tipus ppm/txt: aixo et treura els fitxers ppm/txt!
        int ret=0;
        if(/*!contains_compressed && */pf.getnum()>0){
            myPopup mp;
            if(contains_compressed)mp = new myPopup(350,120,"<html>Tens un fitxer comprimit seleccionat.<br/> Aquesta acció el deseleccionarà. Vols seguir?</html>",frame,true);
            else mp = new myPopup(350,120,"<html>Tens fitxers no comprimits seleccionats.<br/> Aquesta acció els deseleccionarà. Vols seguir?</html>",frame,true);
            ret = mp.getRet();
            if(ret==0)pf.ClearPanell();
            else return;
        }
        contains_compressed = true;
        SelectorFitxer sf = new SelectorFitxer(Accio.UNCOMPRESS);
        //sf.frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        pf.UpdatePanell(sf.getFiletree(), sf.getFiles(),frame);
        if (pf.getnum() > 0) {
            button_o.setEnabled(false);
            button_d.setEnabled(true);
            button_c.setEnabled(false);

            pathfinal.setText("mateix nom que el comprimit");
            pathfinal.setForeground(new Color(150,150,150));
        }
    }

    private void PressedButton_clear(){
        pf.ClearPanell();
        pathfinal.setText("Cap path de sortida seleccionat");
        pathfinal.setForeground(new Color(200,40,10));
        button_c.setEnabled(false);
        button_d.setEnabled(false);
        button_o.setEnabled(false);
    }


    private void PressedButton_o(){
        SelectorFitxer sf;
        /*
        if (contains_compressed) sf = new SelectorFitxer(Accio.SAVE_UNCOMPRESS);
        else sf = new SelectorFitxer(Accio.SAVE_COMPRESS);

         */
        sf = new SelectorFitxer(Accio.SAVE_COMPRESS);
        //sf.frame.dispatchEvent(new WindowEvent(frame,WindowEvent.WINDOW_CLOSING));
        ArrayList<String> tmp = sf.getFiletree();
        if(tmp.size()>0) {
            pathfinal.setText(sf.getFiletree().get(0));
            pathfinal.setForeground(new Color(40,200,10));
        }

        if (pf.getnum() > 0) {
            if(contains_compressed) {
                button_d.setEnabled(true);
                button_c.setEnabled(false);
            }else{
                button_c.setEnabled(true);
                button_d.setEnabled(false);
            }
        }
        //pf.UpdatePanell(sf.getFiletree(),sf.getFiles());
    }

    private void PressedButton_c(ActionEvent ae){

        String path_f = pathfinal.getText();
        if (path_f.equals("")){
            myPopup mp = new myPopup(320,120,"Cal seleccionar el fitxer de sortida",frame,false);
            return;
        }

        int numfiles = 0;
        ArrayList<String> paths = new ArrayList<>();
        ArrayList<String> algorismes = new ArrayList<>();
        ArrayList<Double> ratios = new ArrayList<>();


        for(int i=0; i<pf.getnum(); ++i){
            if (pf.getChecked(i)) {
                ++numfiles;
                File f = pf.getFile(i);
                if(!f.isDirectory()) {
                    paths.add(new File(".").toURI().relativize(f.toURI()).getPath());
                    algorismes.add(pf.getAlgorisme(i));
                    if (pf.getAlgorisme(i).equals("JPEG")) {
                        //System.out.println(i);
                        double[] r = pf.getRatios(i);
                        ratios.add(r[0]);
                        ratios.add(r[1]);
                        ratios.add(r[2]);
                    }
                }
            }
        }

        if (numfiles==0){
            myPopup mp = new myPopup(320,120,"Cap fitxer seleccionat",frame,false);
            return;
        }
        else if (numfiles==1){
            String path_i = paths.get(0);
            String splited[] = path_i.split(" ");
            if (splited.length>0) path_i = splited[splited.length-1];
            String algorisme = algorismes.get(0);
            if(algorisme.equals("JPEG")){
                cp.setRatios(ratios.get(0),ratios.get(1),ratios.get(2));
                numThreads nt = new numThreads(320,120,frame);
                int numthreads = Integer.parseInt(nt.getNumThreads());
                cp.setnumthreads(numthreads);
            }
            cp.compress(path_i,algorisme,path_f);
        }else{
            numThreads nt = new numThreads(320,120,frame);
            int numthreads = Integer.parseInt(nt.getNumThreads());
            cp.setnumthreads(numthreads);
            cp.comprimirConjunt(path_f,paths,algorismes,ratios);
        }
        /*
        for(int i=0; i<numfiles; ++i){
            if (pf.getChecked(i)) {
                String path_i = pf.getPath(i);
                String splited[] = path_i.split(" ");
                if (splited.length>0) path_i = splited[splited.length-1];
                String algorisme = pf.getAlgorisme(i);
                if(algorisme.equals("JPEG")){
                    double[] ratios = pf.getRatios(i);
                    cp.setRatios(ratios[0],ratios[1],ratios[2]);
                }
                cp.compress(path_i,algorisme,path_f);
            }
        }*/
    }

    private void PressedButton_d(ActionEvent ae){

        int numfiles = pf.getnum();
        //System.out.println("trying to decompress " + numfiles + " files");
        if (numfiles==0){
            myPopup mp = new myPopup(320,120,"Cap fitxer seleccionat",frame,false);
            return;
        }
        numThreads nt = new numThreads(320,120,frame);
        int numthreads = Integer.parseInt(nt.getNumThreads());
        cp.setnumthreads(numthreads);

        for(int i=0; i<numfiles; ++i){
            if (pf.getChecked(i)){
                String path_i = pf.getPath(i);
                String path_f = "";
                //System.out.println("path_i: " +path_i);
                String[] splited = path_i.split("\\."); //removes extension
                //System.out.println(splited.length);
                for(int j=0; j<splited.length-1; ++j) path_f+=splited[j];
                //System.out.println("path_f: " + path_f);
                cp.decompress(path_i,path_f);
            }
        }
        //System.out.println("adeu!");
       // cp.decompress(pathinicial.getText(),pathfinal.getText());
    }
}


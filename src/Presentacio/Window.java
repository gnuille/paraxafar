/**
 * @author pvizcaino
 */
package Presentacio;

import javax.swing.*;
import java.awt.*;

public class Window {
    protected JFrame frame;
    protected Container pane;
    protected int size_x,size_y;
    protected ControladorPresentacio cp;

    public Window(int x, int y, ControladorPresentacio cp){
        this.cp = cp;
        size_x = x; size_y = y;
        frame = new JFrame("paraXafar");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //default: si es tenca, fer exit.
        frame.setResizable(false);
        //frame.getContentPane().add(emptyLabel, BorderLayout.CENTER); //afegir un component
        frame.setSize(size_x,size_y);
        pane = frame.getContentPane();
    }
    public void show(){
        //frame.pack();
        frame.setVisible(true);
    }

    public JFrame getFrame(){
        return frame;
    }

    public void center(Component c){
        frame.setLocationRelativeTo(c);
    }

    public void hide(){
        frame.setVisible(false);
    }
}

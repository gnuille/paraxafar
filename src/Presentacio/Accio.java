/**
 * @author pvizcaino
 */

package Presentacio;

public enum Accio {
    LOAD, // seleccionar fitxers no comprimits
    SAVE_COMPRESS, // comrpimir un fitxer
    SAVE_UNCOMPRESS, // descomprimir un fitxer
    UNCOMPRESS,  // sleccionar fitxers comprimits
}
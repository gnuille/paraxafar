/**
 * @author pvizcaino
 */
package Presentacio;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

public class PanellFitxers {
    private ArrayList<File> files;
    private ArrayList<String> paths;
    private ArrayList<JSpinner> spinners;
    private ArrayList<JButton> ratios;
    private ArrayList<JCheckBox> checkboxes;


    private JPanel panel,interior;
    private GridBagConstraints c;
    private JScrollPane scrollpane;

    public PanellFitxers(int x, int y){
        paths = new ArrayList<>();
        files = new ArrayList<>();
        spinners = new ArrayList<>();
        checkboxes = new ArrayList<>();
        ratios = new ArrayList<>();
        panel = new JPanel();
        panel.setPreferredSize(new Dimension(x-50,y-50)); //tamany de lo scrollejable
        scrollpane = new JScrollPane(panel);
        panel.setAutoscrolls(true);
        scrollpane.setPreferredSize(new Dimension(x,y)); //tamany del component en si
        interior = new JPanel();

        interior.setBackground(new Color(220,220,220));
        panel.setBackground(new Color(220,220,220));

        c = new GridBagConstraints();
        interior.setLayout(new GridBagLayout());
    }

    public JScrollPane getScroll(){
        return scrollpane;
    }

    public File getFile(int i){return files.get(i);}
    public String getPath(int i){return paths.get(i);}
    public String getAlgorisme(int i){return spinners.get(i).getValue().toString();}
    public boolean getChecked(int i){return checkboxes.get(i).isSelected();}
    public int getnum(){return files.size();}


    public void UpdatePanell(ArrayList<String> addpaths, ArrayList<File> addfiles, JFrame frame){

        int previoussize = paths.size();

        //afegeix els nous fitxers i paths
        paths.addAll(addpaths);
        files.addAll(addfiles);


        c.weightx=1.0;
        //els hi afegeix els spinners, ratios i checkboxes
        for(int i=previoussize; i<paths.size(); ++i){
            //label amb el path
            c.gridy=i;
            c.gridx=0;
            c.fill = GridBagConstraints.HORIZONTAL;
            JLabel l = new JLabel(paths.get(i));
            interior.add(l,c);

            //Ratio, desactivat per defecte
            JButton ratio = new JButton("");
            ratio.setEnabled(false);
            ratio.setVisible(false);
            ratio.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    JPEGratios r = new JPEGratios(320,120,frame);
                    ratio.setText(r.getY() + " " + r.getCb() + " " + r.getCr());
                }
            });

            //Spinner amb els algorismes
            SpinnerListModel AlgModel = null;
            if(files.get(i).isDirectory()) AlgModel = new SpinnerListModel(new String[]{"Dir"});
            else if(files.get(i).isFile()) {
                AlgModel = new SpinnerListModel(new String[]{"Dir"});
                String[] extensio = paths.get(i).split("\\.");
                if (extensio.length>0){
                    //si es ppm, activa el ratio
                    if(extensio[extensio.length-1].equals("ppm")) {
                        AlgModel = new SpinnerListModel(new String[]{"JPEG"});
                        ratio.setText("0.2 0.9 0.9");
                        ratio.setEnabled(true);
                        ratio.setVisible(true);
                    }
                    else if (extensio[extensio.length-1].equals("txt")) AlgModel = new SpinnerListModel(new String[]{"LZ78", "LZSS", "LZW"});
                    else if (extensio[extensio.length-1].equals("xafar")) AlgModel = new SpinnerListModel(new String[]{"unc"});
                    else AlgModel = new SpinnerListModel(new String[]{"error"});
                }
            }
            //afegeix el spinner
            JSpinner algorismes = new JSpinner(AlgModel);
            c.gridx = 1;
            interior.add(algorismes,c);
            spinners.add(algorismes);

            //afegeix el ratio
            c.gridx=2;
            c.insets = new Insets(0,5,0,5);
            c.fill = GridBagConstraints.WEST;
            //c.anchor=GridBagConstraints.WEST;
            interior.add(ratio,c);
            ratios.add(ratio);
            c.insets = new Insets(0,0,0,0);

            //checkbox
            JCheckBox check = new JCheckBox();
            if (files.get(i).isDirectory()) {
                algorismes.setEnabled(false);
            }
            check.setBackground(new Color(220,220,220));
            check.doClick();
            c.gridx=3;
            c.anchor=GridBagConstraints.EAST;
            c.fill=GridBagConstraints.EAST;
            interior.add(check,c);
            checkboxes.add(check);
        }
        interior.setPreferredSize(new Dimension(300,25*paths.size())); //tamany de lo scrollejable
        scrollpane.getViewport().add(interior);
    }

    public void ClearPanell(){
        files.clear();
        paths.clear();
        spinners.clear();
        checkboxes.clear();
        ratios.clear();
        if(interior!=null){
            interior.removeAll();
            scrollpane.getViewport().add(interior);
        }
    }
    public double[] getRatios(int i){
        String r = ratios.get(i).getText();
        String splited[] = r.split(" ");
        double[] ret = {Double.parseDouble(splited[0]),
                        Double.parseDouble(splited[1]),
                        Double.parseDouble(splited[2])};
        return ret;
    }



}

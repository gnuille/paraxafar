/**
 * @author pvizcaino
 */

package Presentacio;

import Domini.ControladorDomini;
import Domini.TipusAlgorisme;


//Thread que controla a domini
public class InterlocutorAmbDomini implements Runnable{
        private ControladorDomini cd;
        private Missatges m;


        public void run(){
            //mentres no rebi un missatge de kill
            while(!m.killthread){

                //Per a no consumir massa CPU; només mira si hi ha missatges nous cada 300ms
                try {
                    Thread.currentThread().sleep(300);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                //si reb un missatge de numthreads ...
                if (m.msg_numthreads){
                    cd.setnumthreads(m.msg_numthreads_n);
                    m.msg_numthreads=false;
                }


                //si reb un missatge de ratios, crida la funcio del Controlador de Domini dels ratios
                if (m.msg_ratios){
                    cd.ratiosJPEG(m.msg_a,m.msg_b,m.msg_c);
                    m.msg_ratios=false;
                }
                //si es demanen les estadistiques globals les retorna
                if (m.msg_eg){
                    m.msg_estglob = cd.retornaEstadistiquesGlobals();
                    m.msg_eg = false;
                }
                //si es demana un bufferedimage les retorna
                if (m.msg_bImg){
                    m.msg_buffImg = cd.retornaBufferedImage(m.msg_buff_path,m.msg_buff_c);
                    m.msg_bImg = false;
                }
                //si es demana un bufferedreader, les retorna
                if (m.msg_bRead){
                    m.msg_buffRead = cd.retornaBufferedRead(m.msg_buff_path,m.msg_buff_c);
                    m.msg_bRead = false;
                }
                if (m.msg_compressconj){
                    try {
                        cd.comprimirConjunt(m.msg_pathfinal,m.msg_pathiniCjt, m.msg_algorismesCjt, m.msg_ratiosCjt);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    /*for(int i=0; i<m.msg_pathiniCjt.length; ++i){
                        System.out.println(m.msg_pathiniCjt[i]);
                    }*/
                    m.msg_compressconj=false;

                }


                //Ídem per a comprimir i descomprimir
                if (m.msg_compress){
                    try {
                        cd.seleccionarFitxer(m.msg_pathini);
                        switch (m.msg_algorisme){
                            case "JPEG" : cd.seleccionarAlgorismeManual(TipusAlgorisme.JPEG); break;
                            case "LZ78" : cd.seleccionarAlgorismeManual(TipusAlgorisme.LZ78); break;
                            case "LZW" : cd.seleccionarAlgorismeManual(TipusAlgorisme.LZW); break;
                            case "LZSS" : cd.seleccionarAlgorismeManual(TipusAlgorisme.LZSS); break;
                            default: throw new Exception();
                        }
                        cd.comprimirSeleccionat(m.msg_pathfinal);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    m.msg_compress=false;
                }

                if (m.msg_decompress){
                    try{
                        cd.seleccionarFitxerComprimit(m.msg_pathini);
                        cd.descomprimirSeleccionat(m.msg_pathfinal);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                   m.msg_decompress = false;
                }

            }
        }

        public InterlocutorAmbDomini(ControladorPresentacio cp){
            cd = new ControladorDomini(cp);
            this.m = Missatges.getInstance();
        }
}
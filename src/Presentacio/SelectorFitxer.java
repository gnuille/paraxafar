/**
 * @author pvizcaino
 */

package Presentacio;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.util.ArrayList;

public class SelectorFitxer{

    private JFileChooser chooser;
    private ArrayList<File> filearray;
    private ArrayList<String> filetree;
//https://www.mkyong.com/swing/java-swing-jfilechooser-example/
    public SelectorFitxer(Accio a) {

        filearray = new ArrayList<File>();
        filetree = new ArrayList<String>();
        chooser = new JFileChooser(".");
        if(a==Accio.LOAD) chooser.setMultiSelectionEnabled(true);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        chooser.setDialogTitle("Select Files/Folders to compress");
        FileNameExtensionFilter filter;
        if(a==Accio.UNCOMPRESS || a==Accio.SAVE_COMPRESS) filter = new FileNameExtensionFilter("xafar", "xafar");
        else filter = new FileNameExtensionFilter("txt and ppm", "ppm", "txt");
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.addChoosableFileFilter(filter);


        if(a==Accio.SAVE_COMPRESS){
            int returnValue = chooser.showSaveDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                filearray.add(chooser.getSelectedFile());
                String path = new File(".").toURI().relativize(filearray.get(0).toURI()).getPath();
                String[] split = path.split("\\.");
                if ( (split.length==0) || !(split[split.length-1].equals("xafar"))) path+=".xafar";
                filetree.add(path);
            }
        }else if (a==Accio.LOAD) {
            int returnValue = chooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File[] files = chooser.getSelectedFiles();
                for (int i = 0; i < files.length; ++i) recursiveFiles(files[i], "");
            }
        }else{ //igual que SAVE pero sense comprovar la extensio
            int returnValue = chooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                filearray.add(chooser.getSelectedFile());
                filetree.add(new File(".").toURI().relativize(filearray.get(0).toURI()).getPath());
            }
        }
    }

    public ArrayList<File> getFiles(){
        return filearray;
    }
    public ArrayList<String> getFiletree(){
        return filetree;
    }

    private void recursiveFiles(File f, String indent) {
        String path =  new File(".").toURI().relativize(f.toURI()).getPath();
        if (f.isFile()) {
            String[] split = path.split("\\.");
            if(split.length>0) {
                String ext = split[split.length - 1];
                if (!(ext.equals("ppm") || ext.equals("txt"))) {
                    System.out.println("EXCEPTION: INVALID FILE SELECTED (wrong extension)");
                } else {
                    filearray.add(f);
                    filetree.add(indent+path);
                }
            }else System.out.println("EXCEPTION: INVALID FILE SELECTED (no extension)");
            return;
        }
        //it's a folder
        filearray.add(f);
        filetree.add(indent+path);
        File[] files = f.listFiles();
        for (int i = 0; i < files.length; ++i) recursiveFiles(files[i], indent + "  ");
    }


}

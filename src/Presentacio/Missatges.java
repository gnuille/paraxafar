/**
 * @author pvizcaino
 */
package Presentacio;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;

public class Missatges {

    private static Missatges instance;


    public boolean killthread=false;
    public boolean msg_compress=false;
    public boolean msg_decompress=false;
    public boolean msg_ratios=false;
    public boolean msg_eg=false;
    public boolean msg_bImg=false;
    public boolean msg_bRead=false;
    public boolean msg_compressconj=false;
    public boolean msg_numthreads=false;

    public int msg_numthreads_n;
    public String[] msg_pathiniCjt,msg_algorismesCjt;
    public double[] msg_ratiosCjt;
    public String msg_pathini,msg_algorisme,msg_pathfinal,msg_buff_path;
    public double msg_a,msg_b,msg_c;
    public BufferedImage msg_buffImg;
    public String msg_buffRead;
    public boolean msg_buff_c;
    public double[][] msg_estglob;

    private Missatges(){
    }

    public static Missatges getInstance(){
        if (instance==null){
            instance = new Missatges();
        }
        return instance;
    }
}

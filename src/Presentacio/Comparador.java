/**
 * @author pvizcaino
 */

package Presentacio;

import javax.swing.*;
import java.awt.*;

public class Comparador extends Window {


    private JPanel panel, interior1, interior2;
    private JScrollPane scroll1, scroll2;

    public Comparador(int x, int y, ControladorPresentacio cp, JComponent a, JComponent b){
        //El comparador té una estructura similar al PanellFitxers
        super(x,y,cp);
        pane.setBackground(new Color(50,50,50));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //default: si es tenca, fer exit.
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        //panel es un marc, scroll es el propi element scrollejable i interior es el seu contingut
        panel = new JPanel(); //marc
        panel.setPreferredSize(new Dimension(x-50,y-50)); //tamany de lo scrollejable
        scroll1 = new JScrollPane(panel);
        scroll2 = new JScrollPane(panel);
        scroll1.setPreferredSize(new Dimension(x/2 - 10,(y*5)/6)); //els tamanys son arbitraris perque quedin bonics
        scroll2.setPreferredSize(new Dimension(x/2 - 10,(y*5)/6));
        scroll1.setAutoscrolls(false);
        scroll2.setAutoscrolls(false);
        interior1 = new JPanel();
        interior2 = new JPanel();

        interior1.setBackground(new Color(220,220,220));
        interior2.setBackground(new Color(220,220,220));
        panel.setBackground(new Color(220,220,220));

        interior1.setLayout(new GridBagLayout());
        interior2.setLayout(new GridBagLayout());

        JLabel comp = new JLabel("Comparació Original / Final");
        comp.setForeground(new Color(255,255,255));
        c.gridwidth = 2;
        c.gridx=0;
        c.gridy=0;
        pane.add(comp,c);

        //interior panells
        c.gridwidth=1;
        c.gridx=0;
        c.gridy=0;
        c.weighty=1.0;
        c.weightx=1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        interior1.add(a,c);
        interior2.add(b,c);

        //afegir interior als scrolls
        interior1.setPreferredSize(new Dimension(x/2 - 15,(y*5)/6)); //tamany de lo scrollejable
        interior2.setPreferredSize(new Dimension(x/2 - 15,(y*5)/6)); //tamany de lo scrollejable
        scroll1.getViewport().add(interior1);
        scroll2.getViewport().add(interior2);


        //afegir els scrolls
        c.gridy=1;
        c.gridx=0;
        pane.add(scroll1,c);
        c.gridx=1;
        pane.add(scroll2,c);

    }
}

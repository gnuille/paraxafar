/**
 * @author pvizcaino
 */
package Presentacio;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

//Semblant al myPopup fins que no tingui animacions, estadistiques, etc...
public class Process extends Window {

    private JLabel label;
    private JLabel progress;
    private JLabel ratio_label,temps_label;
    private JButton exitbutton,compare;
    private String pathini,pathfin;
    int tipus = 0; //0 = descompressio o compressio carpeta. 1 = compressio imatge. 2 = compressio fitxer
    public Process(int x, int y, ControladorPresentacio cp, String msg, int tipus, String pathini, String pathfin){
        super(x, y, cp);
        this.tipus = tipus;
        this.pathfin=pathfin;
        this.pathini=pathini;
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;

        label = new JLabel(msg);
        c.gridx=0;
        c.gridy=0;
        pane.add(label,c);

        progress = new JLabel("0%");
        c.gridx=1;
        c.gridy=0;
        pane.add(progress,c);
        c.anchor = GridBagConstraints.CENTER;
        c.fill = GridBagConstraints.CENTER;

        JLabel estadistiques = new JLabel("Estadistiques:");
        c.insets = new Insets(15,0,0,0);
        c.gridx = 0;
        c.gridwidth = 2;
        c.gridy = 1;
        pane.add(estadistiques,c);
        c.insets = new Insets(0,0,0,0);


        temps_label = new JLabel("          Calculant...           ");
        c.gridx = 0;
        c.gridy = 2;
        pane.add(temps_label,c);

        ratio_label = new JLabel(" ");
        c.gridx = 0;
        c.gridy = 3;
        pane.add(ratio_label,c);


        exitbutton = new JButton("Return");
        exitbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                frame.dispose();
            }
        });
        exitbutton.setEnabled(false);
        c.gridwidth=1;
        c.gridx = 0;
        c.gridy = 4;
        pane.add(exitbutton,c);

        compare = new JButton("Comparar");
        compare.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Compara();
            }
        });
        compare.setEnabled(false);
        c.gridx = 1;
        c.gridy = 4;
        pane.add(compare,c);

    }
    public void UpdateProcess(int percent){
        String percent_s = String.valueOf(percent);
        progress.setText(percent_s+"%");
    }

    //ratio és -1 si és una descompressió
    public void estableixEstadistiques(double ratio, double temps){

        int min = (int)temps/60000;
        int sec = (int)temps/1000;
        int msec = (int)temps%1000;

        if (ratio != -1) ratio_label.setText(String.format("Ratio: %.2f",ratio));
        temps_label.setText(String.format("Temps: %d min %d seg %d milis", min,sec,msec));
    }

    public void Compara(){

        int sizex = 1200;
        int sizey = 800;

        if(tipus==1) { //imatge
            BufferedImage bi_ini = cp.demanaBufferedImage(pathini, false);
            ImageIcon img_ini = new ImageIcon(bi_ini);
            Image prescale = img_ini.getImage();
            int x = prescale.getWidth(null);
            int y = prescale.getHeight(null);
            double multx = (double)(sizex/2) / (double)x;
            double multy = (double)((sizey*3)/4) / (double)y;
            Image postscale;
            if (multx<multy) postscale = prescale.getScaledInstance((int)(x*multx),(int)(y*multx), Image.SCALE_SMOOTH);
            else postscale = prescale.getScaledInstance((int)(x*multy),(int)(y*multy), Image.SCALE_SMOOTH);
            img_ini = new ImageIcon(postscale);
            JLabel lab_ini = new JLabel();
            lab_ini.setIcon(img_ini);
            BufferedImage bi_fin = cp.demanaBufferedImage(pathfin, true);
            ImageIcon img_fin = new ImageIcon(bi_fin);
            prescale = img_fin.getImage();
            x = prescale.getWidth(null);
            y = prescale.getHeight(null);
            multx = (double)(sizex/2) / (double)x;
            multy = (double)((sizey*3)/4) / (double)y;
            if (multx<multy) postscale = prescale.getScaledInstance((int)(x*multx),(int)(y*multx), Image.SCALE_SMOOTH);
            else postscale = prescale.getScaledInstance((int)(x*multy),(int)(y*multy), Image.SCALE_SMOOTH);
            img_fin = new ImageIcon(postscale);
            JLabel lab_fin = new JLabel();
            lab_fin.setIcon(img_fin);


            Comparador comp = new Comparador(sizex, sizey, cp, lab_ini, lab_fin);
            comp.center(frame);
            comp.show();
        }
        else if (tipus==2){ //fitxer
            String br_ini = cp.demanaBufferedReader(pathini, false);
            JTextArea text_ini = new JTextArea();
            text_ini.setLineWrap(true);
            try {
                //text_ini.read(br_ini, "Reading");
                text_ini.append(br_ini);
            }catch(Exception e){
                e.printStackTrace();
            }

            String br_fin = cp.demanaBufferedReader(pathfin, true);
            JTextArea text_fin = new JTextArea();
            text_fin.setLineWrap(true);
            try {
                //text_fin.read(br_fin, "Reading");
                text_fin.append(br_fin);
            }catch(Exception e){
                e.printStackTrace();
            }
            Comparador comp = new Comparador(sizex, sizey, cp, text_ini, text_fin);
            comp.center(frame);
            comp.show();
        }
    }


    public void Completed(){
        if(tipus!=0)compare.setEnabled(true);
        label.setText("Completat! ");
        exitbutton.setEnabled(true);


/*
            JLabel a = new JLabel("aaaaaaa");
            JLabel b = new JLabel("bbbbbbb");
            Comparador comp = new Comparador(800,800,cp,a,b);
            comp.show();
*/

    }

}
